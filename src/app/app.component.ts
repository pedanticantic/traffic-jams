import {Component} from '@angular/core';
import {SimulationState} from "./models/simulation-state";
import {NgForOf, NgIf} from "@angular/common";
import {TrafficJam} from "./models/traffic-jam";
import {Direction} from "./models/direction";
import {SimulationRunner} from "./models/simulation-runner";
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faArrowsSplitUpAndLeft, faCarSide, faCircleArrowRight, faCircleCheck, faCircleXmark, faClock, faCodeMerge, faHashtag, faSpinner, faTableCells, IconDefinition} from '@fortawesome/free-solid-svg-icons';
import {SearchHelper} from "./services/search-helper";
import {GroundCell} from "./models/ground-cell";
import {ForwardPathCell} from "./models/forward-path-cell";
import {TestService} from "./services/test-service";
import {Container} from "./models/container";
import {Rectangle} from "./models/rectangle";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    FontAwesomeModule,
    NgForOf,
    NgIf
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent {
  faCarSide: IconDefinition = faCarSide;
  faCircleArrowRight: IconDefinition = faCircleArrowRight;
  faHashtag: IconDefinition = faHashtag;
  faArrowsSplitUpAndLeft: IconDefinition = faArrowsSplitUpAndLeft;
  faClock: IconDefinition = faClock;
  faSpinner: IconDefinition = faSpinner;
  faCircleXmark: IconDefinition = faCircleXmark;
  faCircleCheck: IconDefinition = faCircleCheck;
  faTableCells: IconDefinition = faTableCells;
  faCodeMerge: IconDefinition = faCodeMerge;
  title = 'traffic-jams';

  simulationState: SimulationState = new SimulationState();
  simulationRunner: SimulationRunner = new SimulationRunner(this.simulationState);

  constructor() {
    if (this.simulationRunner.getTestService().isInTestMode(this.simulationState)) {
      if (! this.simulationRunner.getTestService().runInTestMode(this.simulationState, this.simulationRunner)) {
        return;
      }
    } else {
      let container: Container = new Container(new Rectangle(0, 10 + Math.floor(Math.random() * 31), 0, 10 + Math.floor(Math.random() * 16)));
      let shouldRemoveNextRectangle: boolean = true;
      let numRectangles: number = 2 + Math.floor(Math.random() * 9);
      let corner1: GroundCell|null;
      let corner2: GroundCell|null;
      let rectangle: Rectangle;
      for (let rectangleIndex: number = 0; rectangleIndex < numRectangles; rectangleIndex++) {
        corner1 = container.getAvailableCell();
        corner2 = container.getAvailableCell();
        if (corner1 !== null && corner2 !== null) {
          rectangle = new Rectangle(Math.min(corner1.getX(), corner2.getX()), Math.max(corner1.getX(), corner2.getX()), Math.min(corner1.getY(), corner2.getY()), Math.max(corner1.getY(), corner2.getY()))
          if (shouldRemoveNextRectangle) {
            container.subtractRectangle(rectangle);
          } else {
            container.addRectangle(rectangle);
          }
          shouldRemoveNextRectangle = Math.random() < 0.5;
        }
      }
      this.simulationState.setContainer(container);

      // Create some random jams.
      let jamCount: number = 2 + Math.floor(Math.random() * 3) + Math.floor(Math.random() * 3);

      let startCell: GroundCell|null;
      let trafficJam: TrafficJam;
      let jamPath: ForwardPathCell[];
      for (let nextJam: number = 0; nextJam < jamCount; nextJam++) {
        trafficJam = this.simulationState.createTrafficJam();
        startCell = this.simulationState.getAvailableCell();
        if (startCell === null) {
          return; // It should never get here.
        }

        jamPath = new SearchHelper(this.simulationState).forwardPathSearch(
          5 + Math.floor(Math.random() * 4) + Math.floor(Math.random() * 4),
          startCell,
          Direction.randomDirection()
        );

        this.simulationState.initialisePath(trafficJam, jamPath);
      }
    }

    // Start the simulation running.
    setTimeout(
      () => this.simulationRunner.nextMove(),
      1000
    )
  }

  protected readonly TestService = TestService;
}
