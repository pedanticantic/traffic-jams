import {JamGroundCell} from "./jam-ground-cell";

export class MergeState {
  private pendingMerge: boolean = false;
  private mergePoint: JamGroundCell|null = null;

  public setPendingMerge(mergePoint: JamGroundCell) {
    this.pendingMerge = true;
    this.mergePoint = mergePoint;
  }

  public isPending(): boolean {
    return this.pendingMerge;
  }

  public getMergePoint(): JamGroundCell|null {
    return this.mergePoint;
  }
}
