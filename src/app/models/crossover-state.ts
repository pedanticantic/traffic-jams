export class CrossoverState {
  private pendingCrossover: boolean = false;

  public setPendingCrossover(): void {
    this.pendingCrossover = true;
  }

  public isPending(): boolean {
    return this.pendingCrossover;
  }
}
