import {SimulationState} from "./simulation-state";
import {TrafficJam} from "./traffic-jam";
import {TestService} from "../services/test-service";

export class SimulationRunner {
  private moveCount: number = 0;
  private simulationState: SimulationState;
  private testService: TestService;

  constructor(simulationState: SimulationState) {
    this.simulationState = simulationState;
    this.testService = new TestService();
    this.simulationState.setAvailableTestScenarios(this.testService.getAvailableTestScenarios());

    let slug: string|null = this.getSlugFromUrl();
    if (slug !== null) {
      this.simulationState.setCurrentTestScenarioBySlug(slug);
    }
  }

  // @TODO: This is very hacky. Work out how to do it properly.
  private getSlugFromUrl(): string|null {
    let regex: RegExp = /[?&]([^=#&]+)=([^&#]*)/g;
    let match: RegExpExecArray|null;
    let slug: string|null = null;
    do {
      match = regex.exec(window.location.href);
      if (match !== null) {
        if (match[1] === 'slug') {
          slug = match[2];
        }
      }
    } while (match !== null);

    return slug;
  }

  public getTestService(): TestService {
    return this.testService;
  }

  public nextMove(withRepeat: boolean = true): void {
    let currentJam: TrafficJam = this.simulationState.getCurrentJam();
    let nextInterval: number|null = currentJam.makeMove(this.simulationState, this.testService);
    this.simulationState.removingAllTrailingCellsWithNoVehicle();

    if (nextInterval === null) {
      this.simulationState.nextJam();
      nextInterval = 1000;
    }

    this.moveCount++;
    this.testService.checkMoveState(this.moveCount, this.simulationState);

    if (withRepeat) {
      setTimeout(
        () => this.nextMove(),
        nextInterval * this.simulationState.getSpeedFactor()
      )
    }
  }

  public getMoveCount(): number {
    return this.moveCount;
  }

  public reset(): void {
    this.moveCount = 0;
  }
}
