import {DirectionDelta} from "./direction-delta";

export class SplitDirectionDeltaPair {
  private readonly delta1: DirectionDelta;
  private readonly delta2: DirectionDelta;

  constructor(delta1: DirectionDelta, delta2: DirectionDelta) {
    this.delta1 = delta1;
    this.delta2 = delta2;
  }

  public getDelta1(): DirectionDelta {
    return this.delta1;
  }

  public getDelta2(): DirectionDelta {
    return this.delta2;
  }

  private static shuffle(splitPairs: SplitDirectionDeltaPair[]): SplitDirectionDeltaPair[] {
    for (let i = splitPairs.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [splitPairs[i], splitPairs[j]] = [splitPairs[j], splitPairs[i]];
    }

    return splitPairs;
  }

  public static makeOptions(): SplitDirectionDeltaPair[] {
    let result: SplitDirectionDeltaPair[] = [];
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeClockwise(), DirectionDelta.makeForwards()));
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeClockwise(), DirectionDelta.makeAntiClockwise()));
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeForwards(), DirectionDelta.makeClockwise()));
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeForwards(), DirectionDelta.makeAntiClockwise()));
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()));
    result.push(new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeForwards()));

    return this.shuffle(result);
  }
}
