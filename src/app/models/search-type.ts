export class SearchType {
  private readonly requiredLength: number|null = null;
  private readonly goOnePastTarget: boolean = false;

  private constructor(requiredLength: number|null, goOnePastTarget: boolean) {
    this.requiredLength = requiredLength;
    this.goOnePastTarget = goOnePastTarget;
  }

  public static makeForwardPathSearchType(requiredLength: number|null): SearchType {
    return new SearchType(requiredLength, false);
  }

  public static makeSearchForCrossoverType(): SearchType {
    return new SearchType(null, true);
  }

  public static makeSearchForMergeType(): SearchType {
    return new SearchType(null, false);
  }

  public hasRequiredLength(): boolean {
    return this.requiredLength !== null;
  }

  public getRequiredLength(): number {
    return this.requiredLength ?? 0;
  }

  public shouldGoOnePastTarget(): boolean {
    return this.goOnePastTarget;
  }
}
