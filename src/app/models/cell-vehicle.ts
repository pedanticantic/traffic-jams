export class CellVehicle {
  private readonly vehicleColour: string;

  constructor(vehicleColour: string) {
    this.vehicleColour = vehicleColour;
  }

  public getColour(): string {
    return this.vehicleColour;
  }
}
