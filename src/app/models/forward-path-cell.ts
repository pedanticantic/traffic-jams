import {GroundCell} from "./ground-cell";
import {Direction} from "./direction";

export class ForwardPathCell {
  private groundCell: GroundCell;
  private direction: Direction;
  constructor(groundCell: GroundCell, direction: Direction) {
    this.groundCell = groundCell;
    this.direction = direction;
  }

  public getGroundCell(): GroundCell {
    return this.groundCell;
  }

  public getDirection(): Direction {
    return this.direction;
  }
}
