import {Direction} from "./direction";
import {CellVehicle} from "./cell-vehicle";

export class GroundCell {
  private readonly x: number;
  private readonly y: number;
  private direction: Direction|null = null; // it's part of a jam iff this.direction is not null.
  private cellVehicle: CellVehicle|null = null;
  private isCrossoverCell: boolean = false;
  private additionalOccupants: number = 0;
  private isSplittingCell: boolean = false;
  private isMergeCell: boolean = false;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  public getX(): number {
    return this.x;
  }

  public getY(): number {
    return this.y;
  }

  public isInJam(): boolean {
    return this.direction !== null;
  }

  public setDirection(direction: Direction|null): void {
    this.direction = direction;
  }

  public getDirection(): Direction|null {
    return this.direction;
  }

  public releaseCell(): void {
    if (this.isMultiOccupancyCrossover()) {
      this.additionalOccupants--;

      return;
    }

    if (this.isCrossover()) {
      this.isCrossoverCell = false;
    }

    if (this.isSplitting()) {
      this.isSplittingCell = false;
    }

    this.setDirection(null);

    this.setVehicle(null);
  }

  public getRotationAngle(): number {
    if (this.isCrossoverCell) {
      return 0;
    }

    let result: number = this.direction && this.direction.getAngleDegrees() ? this.direction.getAngleDegrees() : 0;
    if (this.isSplitting()) {
      result += 225;
    } else {
      if (this.isMerging()) {
        result = 90 - result;
      }
    }

    return result;
}

  public addVehicle(jamColour: string): void {
    this.setVehicle(new CellVehicle(jamColour));
  }

  public setVehicle(cellVehicle: CellVehicle|null): void {
    this.cellVehicle = cellVehicle;
  }

  public hasVehicle(): boolean {
    return this.cellVehicle !== null;
  }

  public getVehicle(): CellVehicle|null {
    return this.cellVehicle;
  }

  public setCrossover(): void {
    this.isCrossoverCell = true;
    this.incrementAdditionalOccupants();
  }

  public incrementAdditionalOccupants(): void {
    this.additionalOccupants++;
  }

  public isCrossover(): boolean {
    return this.isCrossoverCell;
  }

  public isMultiOccupancyCrossover(): boolean {
    return this.isCrossover() && this.additionalOccupants > 0;
  }

  public setSplitting(): void {
    this.isSplittingCell = true;
  }

  public isSplitting(): boolean {
    return this.isSplittingCell;
  }

  public setMerge(): void {
    this.isMergeCell = true;
  }

  public isMerging(): boolean {
    return this.isMergeCell;
  }

  public getVehicleColour(): string {
    return this.cellVehicle?.getColour() ?? 'black';
  }

  public getStateSummary(): string {
    return this.getX() + ', ' +
      this.getY() + ', ' +
      (this.direction?.getAngleDegrees() ?? '') + ', ' +
      this.hasVehicle() + ', ' +
      this.isCrossover();
  }
}
