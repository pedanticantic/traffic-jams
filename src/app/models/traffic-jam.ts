import {SearchHelper} from "../services/search-helper";
import {GroundCell} from "./ground-cell";
import {SimulationState} from "./simulation-state";
import {Direction} from "./direction";
import {ForwardPathCell} from "./forward-path-cell";
import {TestService} from "../services/test-service";
import {ExpectedGroundCell} from "./test-models/expected-ground-cell";
import {SplitDirectionDeltaPair} from "./split-direction-delta-pair";
import {JamSplitting} from "./jam-splitting";
import {JamGroundCell} from "./jam-ground-cell";
import {CrossoverState} from "./crossover-state";
import { MergeState } from "./merge-state";

export class TrafficJam {
  private readonly forwardSearchDistance: number = 5; // How many cells long is the forward search path?
  private readonly vehicleColour: string;
  private firstJamGroundCell: JamGroundCell|null = null;
  private lastDieRoll: number|null = null;
  private movingVehicleCell: JamGroundCell|null = null;
  private crossoverState: CrossoverState|null = null;
  private jamSplitting: JamSplitting|null = null;
  private mergeState: MergeState|null = null;

  constructor() {
    this.vehicleColour = '#';
    for (let colourIndex: number = 0; colourIndex < 3; colourIndex++) {
      this.vehicleColour += Math.floor(Math.random() * (256 * (3 / 4))).toString(16).padStart(2, '0');
    }
  }

  public getLastDieRoll(): number|null {
    let secondGroundCell: GroundCell|null = this.getNthJamGroundCell(1)?.getGroundCell() ?? null;
    if (this.isPendingCrossover() && secondGroundCell?.isCrossover() && ! secondGroundCell?.hasVehicle()) {
      this.clearPendingCrossover();
      this.lastDieRoll = null;
    }

    return this.lastDieRoll;
  }

  public makeMove(simulationState: SimulationState, testService: TestService): number|null {
// console.log('makeMove()...');
    if (this.getLastDieRoll() === null) {
// console.log('Last die roll was null');
      this.rollDie(simulationState, testService);

      return 1000;
    }

    if (this.movingVehicleCell === null && ! this.isSplitting()) {
      return this.handleDieRoll(simulationState, testService) ? 1000 : null;
    }

// console.log('Moving vehicle');
    this.moveVehicle(simulationState);
    // @ts-ignore this.isSplitting() checks that this.jamSplitting is not null
    if (this.movingVehicleCell !== null && (! this.isSplitting() || ! this.jamSplitting.isSwitchingToNextJam())) {
// console.log('There are still vehicles to move');
      return 200;
    }

// console.log('Finished!');
    return null;
  }

  private rollDie(simulationState: SimulationState, testService: TestService): void {
    let minDieRoll: number = 1;
    let maxDieRoll: number = 6;

    this.lastDieRoll = testService.nextDieRoll(simulationState);
    if (this.lastDieRoll === null) {
      this.lastDieRoll = Math.floor(Math.random() * (maxDieRoll - minDieRoll + 1)) + minDieRoll;
    }
  }

  public static getDieRollDescription(rolledNumber: number): string {
    switch (rolledNumber) {
      case 1:
        return 'move forwards';
      case 2:
        return 'turn left';
      case 3:
        return 'turn right';
      case 4:
        return 'cross over another jam';
      case 5:
        return 'split jam';
      case 6:
        return 'merge into another jam';
      default:
        return 'unknown!';
    }
  }

  /**
   * @param simulationState
   * @param testService
   * @return boolean whether we are able to move or not.
   */
  private handleDieRoll(simulationState: SimulationState, testService: TestService): boolean {
    let currentDirection: Direction|null = this.firstJamGroundCell?.getGroundCell().getDirection() ?? null;
    if (currentDirection === null) {
      return false;
    }

    // If we are waiting for a crossover to become free and there isn't a vehicle on the first cell, and the 2nd cell
    // is a crossover with a vehicle on it, we can't do anything.
    let firstGroundCell: GroundCell|null = this.firstJamGroundCell?.getGroundCell() ?? null;
    let secondGroundCell: GroundCell|null = this.firstJamGroundCell?.getNextJamGroundCell()?.getGroundCell() ?? null;
    if (this.isPendingCrossover() &&
      ! firstGroundCell?.hasVehicle() &&
      secondGroundCell?.isCrossover() && secondGroundCell?.hasVehicle()) {
      return false;
    }

    switch (this.lastDieRoll) {
      case 1:
        return this.probeDirectionForwards(simulationState, currentDirection);
      case 2:
        return this.probeDirectionForwards(simulationState, currentDirection.makeAntiClockwise());
      case 3:
        return this.probeDirectionForwards(simulationState, currentDirection.makeClockwise());
      case 4:
        return this.probeDirectionCrossover(simulationState, currentDirection);
      case 5:
        let successfulSplit: boolean = this.probeSplit(simulationState, currentDirection, testService);
        if (! successfulSplit) {
          this.lastDieRoll = null;
        }

        return successfulSplit;
      case 6:
        let successfulMerge: boolean = this.probeMerge(simulationState, currentDirection);
        if (! successfulMerge) {
          this.lastDieRoll = null;
        }

        return successfulMerge;
      default:
        return false;
    }
  }

  /**
   * @param simulationState
   * @param inDirection
   * @return boolean Did we find a forward path?
   */
  private probeDirectionForwards(simulationState: SimulationState, inDirection: Direction): boolean {
    if (this.firstJamGroundCell === null) {
      return false;
    }

    // @TODO: Need to work out how to inject the search service, etc.
    // @TODO: Does this need to return false if there's no path available?
    this.appendForwardPath(
      simulationState,
      new SearchHelper(simulationState)
        .forwardPathSearch(this.forwardSearchDistance, this.firstJamGroundCell.getGroundCell(), inDirection)
    );

    return true;
  }

  /**
   *
   * @param simulationState
   * @param inDirection
   * @private
   * @return boolean whether there is an available path.
   */
  private probeDirectionCrossover(simulationState: SimulationState, inDirection: Direction): boolean {
    if (this.firstJamGroundCell === null) {
      return false;
    }

    let forwardPath: ForwardPathCell[] = new SearchHelper(simulationState).searchForJamToCrossOver(this.firstJamGroundCell.getGroundCell(), inDirection);
    if (forwardPath.length) {
      this.setPendingCrossover();
      forwardPath[forwardPath.length - 2].getGroundCell().setCrossover();
      this.appendForwardPath(simulationState, forwardPath);

      return true;
    }

    // No possible jams to cross over.
    this.lastDieRoll = null;
    this.clearPendingCrossover();

    return false;
  }

  private probeSplit(simulationState: SimulationState, currentDirection: Direction, testService: TestService): boolean {
    if (simulationState.getCurrentJam().getVehicleCount() < 2) {
      return false;
    }
    if (this.firstJamGroundCell === null) {
      return false;
    }

    let options: SplitDirectionDeltaPair[] = testService.getNextSplitDirectionDeltaPairs();
    if (options.length === 0) {
      options = SplitDirectionDeltaPair.makeOptions();
    }

    for (let optionIndex in options) {
      let split1path: ForwardPathCell[] = new SearchHelper(simulationState)
        .forwardPathSearch(
          this.forwardSearchDistance,
          this.firstJamGroundCell.getGroundCell(),
          options[optionIndex].getDelta1().makeDirection(currentDirection)
        );
      if (split1path.length < this.forwardSearchDistance) {
        continue;
      }

      let coordinatesToAvoid: number[][] = split1path.map(
        function (forwardPathCell: ForwardPathCell): number[] {
          return [forwardPathCell.getGroundCell().getX(), forwardPathCell.getGroundCell().getY()];
        }
      )

      let split2path: ForwardPathCell[] = new SearchHelper(simulationState)
        .forwardPathSearch(
          this.forwardSearchDistance,
          this.firstJamGroundCell.getGroundCell(),
          options[optionIndex].getDelta2().makeDirection(currentDirection),
          coordinatesToAvoid
        );
      if (split2path.length === this.forwardSearchDistance) {
        // We have a viable split!
        this.firstJamGroundCell.getGroundCell().setSplitting();
        let forkedJam: TrafficJam = simulationState.forkJam(this, this.getJamGroundCells());

        this.appendForwardPath(simulationState, split1path);
        forkedJam.appendForwardPath(simulationState, split2path);
        this.setPointsForThisJam();

        return true;
      }
    }

    // There are no viable paths.
    return false;
  }

  private probeMerge(simulationState: SimulationState, currentDirection: Direction): boolean {
    if (this.firstJamGroundCell === null) {
      return false;
    }

    let forwardPath: ForwardPathCell[] = new SearchHelper(simulationState)
      .searchForJamToMergeInTo(this.firstJamGroundCell.getGroundCell(), currentDirection);
    if (forwardPath.length) {
      let mergePoint: ForwardPathCell|null = forwardPath.pop() ?? null; // Will never be null - SA.
      if (mergePoint) {
        let mergePointGroundCell: JamGroundCell|null = simulationState.findJamGroundCellForGroundCell(mergePoint.getGroundCell());
        // It should always be non-null - SA.
        if (mergePointGroundCell !== null) {
          this.setPendingMerge(mergePointGroundCell);
        }
        mergePoint.getGroundCell().setMerge();
      }
      this.appendForwardPath(simulationState, forwardPath);

      return true;
    }

    // No possible jams to cross over.
    this.lastDieRoll = null;
    this.clearPendingMerge();

    return false;
  }

  private appendForwardPath(simulationState: SimulationState, forwardPath: ForwardPathCell[]): void {
    for (let fpIndex in forwardPath) {
      simulationState.addForwardPathCellToJam(this, forwardPath[fpIndex], false);
    }

    this.findAndSetNextVehicle();
  }

  public isMovingVehicles(): boolean {
    return this.movingVehicleCell !== null;
  }

  public findAndSetNextVehicle(shouldMoveIndex: boolean = true): void {
    this.setPointsForThisJam();

    let originalStartJamGroundCell: JamGroundCell|null = this.firstJamGroundCell;
    this.movingVehicleCell = null;

    if (originalStartJamGroundCell === null) {
      return;
    }

    if (this.isPendingCrossover() &&
      ! this.firstJamGroundCell?.getGroundCell().hasVehicle()) {
      originalStartJamGroundCell = this.firstJamGroundCell?.getNextJamGroundCell()?.getNextJamGroundCell() ?? null;
    }
    if (originalStartJamGroundCell === null) {
      return;
    }

    let originalPointsSignature: string = originalStartJamGroundCell.makePointsSignature();

    // This is quite complicated. We need to traverse the jam, changing points as we go, until we find a vehicle to
    // move. If we don't find one, we take a new signature. If it's the same as the original, there are no possible
    // vehicles to move. Otherwise, we repeat (any points will be different). We keep going until we find a vehicle to
    // move, or we have the same signature as the original (meaning there are no possible vehicles to move).
    let vehicleJamGroundCell: JamGroundCell|null;
    do {
      vehicleJamGroundCell = this.findVehicleForCurrentPointsPositions(originalStartJamGroundCell, shouldMoveIndex);
      if (vehicleJamGroundCell) {
        this.movingVehicleCell = vehicleJamGroundCell;

        return;
      }
    } while (originalPointsSignature !== originalStartJamGroundCell.makePointsSignature());

    if (! shouldMoveIndex) {
      return this.findAndSetNextVehicle();
    }

    return;
  }

  private findVehicleForCurrentPointsPositions(startJamGroundCell: JamGroundCell, shouldMoveIndex: boolean = true): JamGroundCell|null {
    // Now, to find the next vehicle to move, we traverse the jam (changing points as we go). We must wait until we
    // find a cell containing a vehicle, and then find a cell not containing a vehicle, and then if/when we find a
    // cell containing a vehicle, that is the one to move(!). In certain cases, we assume we've seen the first vehicle.
    let currentJamGroundCell: JamGroundCell|null = startJamGroundCell;
    let foundVehicle: boolean = ! currentJamGroundCell.getGroundCell().hasVehicle();
    let foundEmptyCell: boolean = false;

    while (currentJamGroundCell !== null) {
      if (! foundVehicle) {
        if (currentJamGroundCell.getGroundCell().hasVehicle()) {
          foundVehicle = true;
        }
      } else {
        if (! foundEmptyCell) {
          if (! currentJamGroundCell.getGroundCell().hasVehicle() && ! currentJamGroundCell.getGroundCell().isCrossover()) {
            foundEmptyCell = true;
          }
        } else {
          if (currentJamGroundCell.getGroundCell().hasVehicle()) {
            return currentJamGroundCell;
          }
        }
      }

      currentJamGroundCell = currentJamGroundCell.getNextJamGroundCell(shouldMoveIndex);
    }

    return null;
  }

  public moveVehicle(simulationState: SimulationState): void {
    if (this.movingVehicleCell === null) {
      return; // Just to satisfy SA.
    }

    // Look at the vehicle we're currently moving.
    // If next cell is empty, move our vehicle to there.
    if (this.allowedToEnterCell(this.movingVehicleCell)) {
      // Move vehicle from this cell to the preceding cell.
      let vehicleCurrentCell: GroundCell = this.movingVehicleCell.getGroundCell();
      let vehiclePreviousCell: GroundCell|null = this.movingVehicleCell.getPreviousJamGroundCell()?.getGroundCell() ?? null;
      if (vehiclePreviousCell) {
        vehiclePreviousCell.setVehicle(vehicleCurrentCell.getVehicle());
        vehicleCurrentCell.setVehicle(null);
      }

      // Vehicle is now one position ahead of where it was.
      this.movingVehicleCell = this.movingVehicleCell.getPreviousJamGroundCell();

      return;
    }

    this.findAndSetNextVehicle();
    if (this.movingVehicleCell) {
      if (this.isSplitting()) {
        // @ts-ignore this.isSplitting() checks that this.jamSplitting is not null
        this.jamSplitting.setSwitchToNextJam();
      }

      return;
    }

    if (this.isSplitting()) {
      // Tell the simulation state we've finished splitting this jam.
      simulationState.handleForkingFinished(this);

      // Clear splitting for _both_ jams.
      // @ts-ignore this.isSplitting() checks that this.jamSplitting is not null
      this.jamSplitting.getOtherJam().clearSplitting();
      this.clearSplitting();
    } else {
      if (this.isPendingMerge() && this.mergeState !== null) {
        // Merge this jam's cells into the jam we're mering with and destroy the jam.
        let mergePoint: JamGroundCell|null = this.mergeState.getMergePoint();
        if (mergePoint !== null && this.firstJamGroundCell !== null) {
          mergePoint.appendNextJamGroundCell(this.firstJamGroundCell);
          this.firstJamGroundCell.appendPreviousJamGroundCell(mergePoint);
          this.firstJamGroundCell = null;

          simulationState.removeJam(this);
        }
      }
    }

    // No more vehicles left to move - this traffic jam has finished its move.
    // If we are waiting for a crossover and there isn't a vehicle on the first cell, don't clear the die roll.
    if (this.isPendingCrossover() && ! this.firstJamGroundCell?.getGroundCell().hasVehicle()) {
      return;
    }

    this.lastDieRoll = null;
  }

  private allowedToEnterCell(fromCell: JamGroundCell): boolean {
    let previousCell: JamGroundCell|null = fromCell.getPreviousJamGroundCell();
    let groundCell: GroundCell|null = previousCell?.getGroundCell() ?? null;
    if (groundCell === null) {
      return false;
    }

    if (groundCell.hasVehicle()) {
      return false;
    }
    if (! groundCell.isMultiOccupancyCrossover()) {
      return true;
    }

    return previousCell !== null && this.allowedToEnterCell(previousCell);
  }

  public getGroundCellCoordinates(): number[][] {
    let jamGroundCells: JamGroundCell[] = this.getJamGroundCells();

    return jamGroundCells.map(
      (jamGroundCell: JamGroundCell) => [jamGroundCell.getGroundCell().getX(), jamGroundCell.getGroundCell().getY()]
    );
  }

  public getGroundCellStates(): string {
    let jamGroundCells: JamGroundCell[] = this.getJamGroundCells();

    return jamGroundCells
      .map((jamGroundCell: JamGroundCell): string => '(' + jamGroundCell.getGroundCell().getStateSummary() + ')')
      .join(', ');
  }

  public isWaitingForCrossover(): boolean {
    return this.lastDieRoll === 4 && this.isPendingCrossover();
  }

  public checkCellStateAt(x: number, y: number, expectedGroundCell: ExpectedGroundCell): boolean {
    // Find the cell in the jam with the given coordinates.
    let jamGroundCells: JamGroundCell[] = this.getJamGroundCells().filter(
      (groundCell: JamGroundCell): boolean => groundCell.getGroundCell().getX() === x && groundCell.getGroundCell().getY() === y
    );
    if (jamGroundCells.length === 0) {
      return false;
    }
    let groundCell: GroundCell = jamGroundCells[0].getGroundCell();

    // Now check any non-null expected values.
    return (expectedGroundCell.getDirectionAngle() === null || expectedGroundCell.getDirectionAngle() === groundCell.getDirection()?.getAngleDegrees()) &&
      (expectedGroundCell.getHasVehicle() === null || expectedGroundCell.getHasVehicle() === groundCell.hasVehicle()) &&
      (expectedGroundCell.getIsCrossoverCell() === null || expectedGroundCell.getIsCrossoverCell() === groundCell.isCrossover());
  }

  // @TODO: shouldNotHaveGroundCell is unused. Ah, it's because it'll be used in the 2nd half of the method, when it's written.
  // @TODO: Bear in mind, no calls to this method supply the value.
  public shouldNotHaveCellAt(x: number, y: number, shouldNotHaveGroundCell: ExpectedGroundCell): boolean {
    // Find the cell in the jam with the given coordinates.
    let groundCells: GroundCell[] = this.getJamGroundCells()
      .map((jamGroundCell: JamGroundCell) => jamGroundCell.getGroundCell())
      .filter((groundCell: GroundCell): boolean => groundCell.getX() === x && groundCell.getY() === y
    );
    if (groundCells.length === 0) {
      return true; // No cell at all, so that's good
    }

    // @TODO: Finish this off (will be similar to the 2nd half of checkCellStateAt() above).
    return false; // @TODO: Assume failure for now.
    // return true; // @TODO: Temporary
  }

  private setPendingCrossover(): void {
    if (this.crossoverState === null) {
      this.crossoverState = new CrossoverState();
    }
    this.crossoverState.setPendingCrossover();
  }

  private clearPendingCrossover(): void {
    this.crossoverState = null;
  }

  private isPendingCrossover(): boolean {
    return this.crossoverState !== null && this.crossoverState.isPending();
  }

  public setJamSplitting(jamSplitting: JamSplitting): void {
    this.jamSplitting = jamSplitting;
    this.lastDieRoll = 5;
  }

  private clearSplitting(): void {
    this.jamSplitting = null;
    this.lastDieRoll = null;
    // Remove any trailing "orphaned" cells.
    this.removingTrailingCellsWithNoVehicle();
    this.movingVehicleCell = null;
  }

  public isSplitting(): boolean {
    return this.jamSplitting !== null;
  }

  public getJamSplitting(): JamSplitting|null {
    return this.jamSplitting;
  }

  private setPendingMerge(mergePoint: JamGroundCell): void {
    if (this.mergeState === null) {
      this.mergeState = new MergeState();
    }
    this.mergeState.setPendingMerge(mergePoint);
  }

  private clearPendingMerge(): void {
    this.mergeState = null;
  }

  private isPendingMerge(): boolean {
    return this.mergeState !== null && this.mergeState.isPending();
  }

  public removingTrailingCellsWithNoVehicle(): void {
    if (this.firstJamGroundCell === null) {
      return;
    }

    let leafNodes: JamGroundCell[];
    do {
      leafNodes = this.firstJamGroundCell.getLeafNodes()
        .filter((leafNode: JamGroundCell): boolean => ! leafNode.getGroundCell().hasVehicle());

      leafNodes.forEach((jamGroundCell: JamGroundCell) => jamGroundCell.releaseThisAndAllAfter());

    } while (leafNodes.length > 0);
  }

  private getVehicleCount(): number {
    let vehicleCount: number = 0;

    let jamGroundCell: JamGroundCell|null = this.firstJamGroundCell;
    while (jamGroundCell) {
      vehicleCount += jamGroundCell.getGroundCell().hasVehicle() ? 1 : 0;
      jamGroundCell = jamGroundCell.getNextJamGroundCell();
    }

    return vehicleCount;
  }

  public getFirstJamGroundCell(): JamGroundCell|null {
    return this.firstJamGroundCell;
  }

  public getFirstCell(): GroundCell {
    // @ts-ignore
    return this.firstJamGroundCell.getGroundCell();
  }

  public getVehicleColour(): string {
    return this.vehicleColour;
  }

  public releaseAllCells(): void {
    if (this.firstJamGroundCell) {
      this.firstJamGroundCell.releaseThisAndAllAfter();
    }
  }

  private getNthJamGroundCell(index: number): JamGroundCell|null {
    let result: JamGroundCell|null = this.firstJamGroundCell; // index 0.
    while (index > 0 && result !== null) {
      result = result.getNextJamGroundCell();
      --index;
    }

    return result;
  }

  private getJamGroundCells(): JamGroundCell[] {
    return this.getJamGroundCellsFromCell(this.firstJamGroundCell, []);
  }

  private getJamGroundCellsFromCell(fromCell: JamGroundCell|null, cellsSoFar: JamGroundCell[]): JamGroundCell[] {
    if (fromCell !== null) {
      cellsSoFar.push(fromCell);

      let nextJamGroundCells: JamGroundCell[] = fromCell.getNextJamGroundCells();
      for (let nextIndex: number = 0; nextIndex < nextJamGroundCells.length; nextIndex++) {
        this.getJamGroundCellsFromCell(nextJamGroundCells[nextIndex], cellsSoFar);
      }
    }

    return cellsSoFar;
  }

  public setFirstJamGroundCell(jamGroundCell: JamGroundCell): void {
    this.firstJamGroundCell = jamGroundCell;
  }

  public clearFirstJamGroundCell(): void {
    this.firstJamGroundCell = null;
  }

  public setPointsForThisJam(): void {
    if (this.firstJamGroundCell) {
      this.firstJamGroundCell.setPreviousPoints(null);
    }
  }
}
