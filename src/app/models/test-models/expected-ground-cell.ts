export class ExpectedGroundCell {
  private x: number;
  private y: number;
  private directionAngle: number|null;
  private hasVehicle: boolean|null;
  private isCrossoverCell: boolean|null;

  constructor(
    x: number,
    y: number,
    directionAngle: number|null,
    hasVehicle: boolean|null,
    isCrossoverCell: boolean|null
  ) {
    this.x = x;
    this.y = y;
    this.directionAngle = directionAngle;
    this.hasVehicle = hasVehicle;
    this.isCrossoverCell = isCrossoverCell;
  }

  public getX(): number {
    return this.x;
  }

  public getY(): number {
    return this.y;
  }

  public getDirectionAngle(): number|null {
    return this.directionAngle;
  }

  public getHasVehicle(): boolean|null {
    return this.hasVehicle;
  }

  public getIsCrossoverCell(): boolean|null {
    return this.isCrossoverCell;
  }
}
