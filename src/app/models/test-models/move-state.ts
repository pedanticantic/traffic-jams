import {TrafficJamState} from "./traffic-jam-state";

const pending: string = 'Pending';
const inProgress: string = 'In Progress';
const failed: string = 'Failed!';
const passed: string = 'Passed!';

export class MoveState {
  private moveNumber: number;
  private expectedJamCount: number;
  private trafficJamStates: TrafficJamState[];
  private outcome: string;
  private shouldGoSlow: boolean;

  constructor(moveNumber: number, expectedJamCount: number, trafficJamStates: TrafficJamState[], shouldGoSlow: boolean = false) {
    this.moveNumber = moveNumber;
    this.expectedJamCount = expectedJamCount;
    this.trafficJamStates = trafficJamStates;
    this.shouldGoSlow = shouldGoSlow;
    this.outcome = pending;
  }

  public getMoveNumber(): number {
    return this.moveNumber;
  }

  public getExpectedJamCount(): number {
    return this.expectedJamCount;
  }

  public getTrafficJamStates(): TrafficJamState[] {
    return this.trafficJamStates;
  }

  public getMoveOutcome(): string {
    return this.outcome;
  }

  public isPending(): boolean {
    return this.outcome === pending;
  }

  public isInProgress(): boolean {
    return this.outcome === inProgress;
  }

  public hasFailed(): boolean {
    return this.outcome === failed;
  }

  public hasPassed(): boolean {
    return this.outcome === passed;
  }

  public setMoveOutcomeInProgress(): void {
    this.outcome = inProgress;
  }

  public setMoveOutcomeFail(): void {
    this.outcome = failed;
  }

  public setMoveOutcomePass(): void {
    this.outcome = passed;
  }

  public goSlow(): boolean {
    return this.shouldGoSlow;
  }
}
