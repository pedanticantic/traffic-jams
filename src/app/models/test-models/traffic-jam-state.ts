import {ExpectedGroundCell} from "./expected-ground-cell";

export class TrafficJamState {
  private expectedGroundCells: ExpectedGroundCell[];
  private shouldNotHaveGroundCells: ExpectedGroundCell[];

  constructor(expectedGroundCells: ExpectedGroundCell[], shouldNotHaveGroundCells: ExpectedGroundCell[] = []) {
    this.expectedGroundCells = expectedGroundCells;
    this.shouldNotHaveGroundCells = shouldNotHaveGroundCells;
  }

  public getExpectedGroundCells(): ExpectedGroundCell[] {
    return this.expectedGroundCells;
  }

  public getShouldNotHaveGroundCells(): ExpectedGroundCell[] {
    return this.shouldNotHaveGroundCells;
  }
}
