import {min} from "rxjs";

export class Rectangle {
  private readonly x1:number;
  private readonly x2:number;
  private readonly y1:number;
  private readonly y2:number;

  constructor(x1: number, x2: number, y1: number, y2: number) {
    this.x1 = x1;
    this.x2 = x2;
    this.y1 = y1;
    this.y2 = y2;
  }

  public getX1(): number {
    return this.x1;
  }

  public getX2(): number {
    return this.x2;
  }

  public getY1(): number {
    return this.y1;
  }

  public getY2(): number {
    return this.y2;
  }

  // Return a rectangle object representing the smallest possible rectangle that encloses ourselves and the given
  // rectangle.
  public enclosingRectangle(initialShape: Rectangle) {
    return new Rectangle(
      Math.min(this.x1, initialShape.getX1()),
      Math.max(this.x2, initialShape.getX2()),
      Math.min(this.y1, initialShape.getY1()),
      Math.max(this.y2, initialShape.getY2())
    );
  }
}
