import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split2 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 2';
  }

  public getDescription(): string {
    return 'Splitting a short jam';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 10, 0, 5],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeForwards(), DirectionDelta.makeAntiClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(39, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(9, 5, 90, true, false),
          new ExpectedGroundCell(9, 6, 90, true, false),
        ]),
      ]),
    ];
  }
}
