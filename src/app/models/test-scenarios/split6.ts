import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split6 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 6';
  }

  public getDescription(): string {
    return 'Splitting rolled when only 1 vehicle in jam';
  }

  public getJamConfig(): number[][] {
    return [
      [16, 15, 0, 1],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(10, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(22, 15, 0, true, false),
        ], [
          new ExpectedGroundCell(17, 15, 0, true, false),
        ]),
      ]),
    ];
  }
}
