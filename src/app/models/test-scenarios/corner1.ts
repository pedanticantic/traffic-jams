import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Corner1 extends AbstractTestScenario {
  public getName(): string {
    return 'Corner 1';
  }

  public getDescription(): string {
    return '1 jam, in corner, trying to move forwards';
  }

  public getJamConfig(): number[][] {
    return [
      [20, 19, 0, 9],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [1];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(55, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 14, 90, true, false),
          new ExpectedGroundCell(29, 15, 90, true, false),
          new ExpectedGroundCell(29, 16, 90, true, false),
          new ExpectedGroundCell(29, 17, 90, true, false),
          new ExpectedGroundCell(29, 18, 90, true, false),
          new ExpectedGroundCell(29, 19, 0, true, false),
          new ExpectedGroundCell(28, 19, 0, true, false),
          new ExpectedGroundCell(27, 19, 0, true, false),
          new ExpectedGroundCell(26, 19, 0, true, false),
        ]),
      ]),
    ];
  }
}
