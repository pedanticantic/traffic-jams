import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Merge8 extends AbstractTestScenario {
  public getName(): string {
    return 'Merge 8';
  }

  public getDescription(): string {
    return 'One jam splits and then merges';
  }

  public getJamConfig(): number[][] {
    return [
      [0, 12, 0, 23],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  public override getDieRollSequence(): number[] {
    return [5, 3, 6, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeForwards()),
      ],
    ];
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(396, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(28, 7, 0, true, false),
          new ExpectedGroundCell(27, 7, 0, true, false),
          new ExpectedGroundCell(26, 7, 0, true, false),
          new ExpectedGroundCell(25, 7, 0, true, false),
          new ExpectedGroundCell(24, 7, 0, true, false),
          new ExpectedGroundCell(23, 7, 90, true, false),
          new ExpectedGroundCell(23, 8, 90, true, false),
          new ExpectedGroundCell(23, 9, 90, true, false),
          new ExpectedGroundCell(23, 10, 90, true, false),
          new ExpectedGroundCell(23, 11, 90, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, false, false),
          new ExpectedGroundCell(20, 12, 0, false, false),
          new ExpectedGroundCell(19, 12, 0, false, false),
          new ExpectedGroundCell(18, 12, 0, false, false),
          new ExpectedGroundCell(17, 12, 0, true, false),
          new ExpectedGroundCell(16, 12, 0, true, false)
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(28, 8, 90, true, false),
          new ExpectedGroundCell(28, 9, 90, true, false),
          new ExpectedGroundCell(28, 10, 90, true, false),
          new ExpectedGroundCell(28, 11, 90, true, false),
          new ExpectedGroundCell(28, 12, 0, true, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, true, false),
          new ExpectedGroundCell(24, 12, 0, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, false, false),
          new ExpectedGroundCell(20, 12, 0, false, false),
          new ExpectedGroundCell(19, 12, 0, false, false),
          new ExpectedGroundCell(18, 12, 0, false, false),
          new ExpectedGroundCell(17, 12, 0, true, false),
          new ExpectedGroundCell(16, 12, 0, true, false)
        ]),
      ]),
      new MoveState(452, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 0, true, false),
          new ExpectedGroundCell(28, 7, 0, true, false),
          new ExpectedGroundCell(27, 7, 0, true, false),
          new ExpectedGroundCell(26, 7, 0, true, false),
          new ExpectedGroundCell(25, 7, 0, false, false),
          new ExpectedGroundCell(24, 7, 0, false, false),
          new ExpectedGroundCell(23, 7, 90, true, false),
          new ExpectedGroundCell(23, 8, 90, true, false),
          new ExpectedGroundCell(23, 9, 90, true, false),
          new ExpectedGroundCell(23, 10, 90, true, false),
          new ExpectedGroundCell(23, 11, 90, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(28, 8, 90, true, false),
          new ExpectedGroundCell(28, 9, 90, true, false),
          new ExpectedGroundCell(28, 10, 90, false, false),
          new ExpectedGroundCell(28, 11, 90, false, false),
          new ExpectedGroundCell(28, 12, 0, false, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, true, false),
          new ExpectedGroundCell(24, 12, 0, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false)
        ]),
      ]),
      new MoveState(491, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 0, true, false),
          new ExpectedGroundCell(28, 7, 0, true, false),
          new ExpectedGroundCell(27, 7, 0, true, false),
          new ExpectedGroundCell(26, 7, 0, true, false),
          new ExpectedGroundCell(25, 7, 0, true, false),
          new ExpectedGroundCell(24, 7, 0, true, false),
          new ExpectedGroundCell(23, 7, 90, true, false),
          new ExpectedGroundCell(23, 8, 90, true, false),
          new ExpectedGroundCell(23, 9, 90, true, false),
          new ExpectedGroundCell(23, 10, 90, true, false),
          new ExpectedGroundCell(23, 11, 90, false, false),
          new ExpectedGroundCell(23, 12, 0, false, false),
          new ExpectedGroundCell(22, 12, 0, false, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(28, 8, 90, true, false),
          new ExpectedGroundCell(28, 9, 90, true, false),
          new ExpectedGroundCell(28, 10, 90, true, false),
          new ExpectedGroundCell(28, 11, 90, true, false),
          new ExpectedGroundCell(28, 12, 0, true, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, false, false),
          new ExpectedGroundCell(24, 12, 0, false, false),
          new ExpectedGroundCell(23, 12, 0, false, false),
          new ExpectedGroundCell(22, 12, 0, false, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false)
        ]),
      ]),
      new MoveState(577, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 0, true, false),
          new ExpectedGroundCell(28, 7, 0, true, false),
          new ExpectedGroundCell(27, 7, 0, true, false),
          new ExpectedGroundCell(26, 7, 0, true, false),
          new ExpectedGroundCell(25, 7, 0, false, false),
          new ExpectedGroundCell(24, 7, 0, false, false),
          new ExpectedGroundCell(23, 7, 90, true, false),
          new ExpectedGroundCell(23, 8, 90, true, false),
          new ExpectedGroundCell(23, 9, 90, true, false),
          new ExpectedGroundCell(23, 10, 90, true, false),
          new ExpectedGroundCell(23, 11, 90, true, false),
          new ExpectedGroundCell(28, 8, 90, true, false),
          new ExpectedGroundCell(28, 9, 90, true, false),
          new ExpectedGroundCell(28, 10, 90, false, false),
          new ExpectedGroundCell(28, 11, 90, false, false),
          new ExpectedGroundCell(28, 12, 0, false, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, true, false)
        ]),
      ]),
      new MoveState(989, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(12, 0, 180, true, false),
          new ExpectedGroundCell(13, 0, 180, true, false),
          new ExpectedGroundCell(14, 0, 180, true, false),
          new ExpectedGroundCell(15, 0, 180, true, false),
          new ExpectedGroundCell(16, 0, 180, true, false),
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 180, true, false),
          new ExpectedGroundCell(20, 0, 180, true, false),
          new ExpectedGroundCell(21, 0, 180, true, false),
          new ExpectedGroundCell(22, 0, 180, true, false),
          new ExpectedGroundCell(23, 0, 180, true, false),
          new ExpectedGroundCell(24, 0, 180, true, false),
          new ExpectedGroundCell(25, 0, 180, true, false),
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false)
        ]),
      ]),
    ];
  }
}
