import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split3 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 3';
  }

  public getDescription(): string {
    return 'Splitting, then splitting again';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 10, 0, 14],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [5, 5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()),
      ],
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeForwards()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(207, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(13, 5, 180, true, false),
          new ExpectedGroundCell(14, 5, 180, true, false),
          new ExpectedGroundCell(15, 5, 180, true, false),
          new ExpectedGroundCell(16, 5, 180, true, false),
          new ExpectedGroundCell(17, 5, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(18, 0, 90, true, false),
          new ExpectedGroundCell(18, 1, 90, true, false),
          new ExpectedGroundCell(18, 2, 90, true, false),
          new ExpectedGroundCell(18, 3, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(18, 15, 270, true, false),
          new ExpectedGroundCell(18, 14, 270, true, false),
          new ExpectedGroundCell(18, 13, 270, true, false),
          new ExpectedGroundCell(18, 12, 270, true, false),
          new ExpectedGroundCell(18, 11, 270, true, false),
        ], [
          new ExpectedGroundCell(18, 10, 0, false, false),
        ]),
      ]),
    ];
  }
}
