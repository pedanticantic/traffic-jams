import {Container} from "../container";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {AbstractTestScenario} from "./abstract-test-scenario";
import {Rectangle} from "../rectangle";

export class Obstacles1 extends AbstractTestScenario {
  public getName(): string {
    return 'Obstacles 1';
  }

  public getDescription(): string {
    return 'A few obstacles that the jam will hit';
  }

  public getJamConfig(): number[][] {
    return [
      [6, 10, 0, 4],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [1];
  }

  protected makeContainer(): Container {
    return new Container(new Rectangle(0, 25, 0, 15))
      .subtractRectangle(new Rectangle(15, 17, 8, 11))
      .subtractRectangle(new Rectangle(12, 16, 3, 5))
      .subtractRectangle(new Rectangle(9, 9, 4, 8))
      .subtractRectangle(new Rectangle(8, 11, 14, 14))
      .subtractRectangle(new Rectangle(25, 25, 9, 9));
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(25, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(14, 9, 90, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
        ]),
      ]),
      new MoveState(51, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(12, 6, 180, true, false),
          new ExpectedGroundCell(13, 6, 180, true, false),
          new ExpectedGroundCell(14, 6, 90, true, false),
          new ExpectedGroundCell(14, 7, 90, true, false),
        ]),
      ]),
      new MoveState(77, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(10, 9, 270, true, false),
          new ExpectedGroundCell(10, 8, 270, true, false),
          new ExpectedGroundCell(10, 7, 270, true, false),
          new ExpectedGroundCell(10, 6, 180, true, false),
        ]),
      ]),
      new MoveState(103, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(11, 13, 0, true, false),
          new ExpectedGroundCell(10, 13, 270, true, false),
          new ExpectedGroundCell(10, 12, 270, true, false),
          new ExpectedGroundCell(10, 11, 270, true, false),
        ]),
      ]),
      new MoveState(181, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(25, 12, 90, true, false),
          new ExpectedGroundCell(25, 13, 0, true, false),
          new ExpectedGroundCell(24, 13, 0, true, false),
          new ExpectedGroundCell(23, 13, 0, true, false),
        ]),
      ]),
      new MoveState(207, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(22, 10, 180, true, false),
          new ExpectedGroundCell(23, 10, 180, true, false),
          new ExpectedGroundCell(24, 10, 180, true, false),
          new ExpectedGroundCell(25, 10, 90, true, false),
        ]),
      ]),
      new MoveState(233, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(18, 11, 270, true, false),
          new ExpectedGroundCell(18, 10, 180, true, false),
          new ExpectedGroundCell(19, 10, 180, true, false),
          new ExpectedGroundCell(20, 10, 180, true, false),
        ]),
      ]),
      new MoveState(259, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(19, 15, 0, true, false),
          new ExpectedGroundCell(18, 15, 270, true, false),
          new ExpectedGroundCell(18, 14, 270, true, false),
          new ExpectedGroundCell(18, 13, 270, true, false),
        ]),
      ]),
    ];
  }
}
