import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover5 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 5';
  }

  public getDescription(): string {
    return '2 crossovers going through 1 jam, consecutive cells';
  }

  public getJamConfig(): number[][] {
    return [
      [7, 10, 0, 8],
      [25, 10, 180, 8],
      [16, 1, 270, 8],
      [7, 11, 0, 16],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [4, 4, 4, 1];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(24, 4, [
        new TrafficJamState([
          new ExpectedGroundCell(15, 12, 270, false, false),
          new ExpectedGroundCell(15, 11, 0, true, true),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
          new ExpectedGroundCell(11, 10, 0, true, false),
          new ExpectedGroundCell(10, 10, 0, true, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 12, 270, false, false),
          new ExpectedGroundCell(17, 11, 0, true, true),
          new ExpectedGroundCell(17, 10, 180, true, false),
          new ExpectedGroundCell(18, 10, 180, true, false),
          new ExpectedGroundCell(19, 10, 180, true, false),
          new ExpectedGroundCell(20, 10, 180, true, false),
          new ExpectedGroundCell(21, 10, 180, true, false),
          new ExpectedGroundCell(22, 10, 180, true, false),
          new ExpectedGroundCell(23, 10, 180, true, false),
          new ExpectedGroundCell(24, 10, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(16, 12, 270, false, false),
          new ExpectedGroundCell(16, 11, 0, true, true),
          new ExpectedGroundCell(16, 10, 270, true, false),
          new ExpectedGroundCell(16, 9, 270, true, false),
          new ExpectedGroundCell(16, 8, 270, true, false),
          new ExpectedGroundCell(16, 7, 270, true, false),
          new ExpectedGroundCell(16, 6, 270, true, false),
          new ExpectedGroundCell(16, 5, 270, true, false),
          new ExpectedGroundCell(16, 4, 270, true, false),
          new ExpectedGroundCell(16, 3, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(23, 11, 0, true, false),
          new ExpectedGroundCell(22, 11, 0, true, false),
          new ExpectedGroundCell(21, 11, 0, true, false),
          new ExpectedGroundCell(20, 11, 0, true, false),
          new ExpectedGroundCell(19, 11, 0, true, false),
          new ExpectedGroundCell(18, 11, 0, true, false),
          new ExpectedGroundCell(17, 11, 0, true, true),
          new ExpectedGroundCell(16, 11, 0, true, true),
          new ExpectedGroundCell(15, 11, 0, true, true),
          new ExpectedGroundCell(14, 11, 0, true, false),
          new ExpectedGroundCell(13, 11, 0, true, false),
          new ExpectedGroundCell(12, 11, 0, true, false),
          new ExpectedGroundCell(11, 11, 0, true, false),
          new ExpectedGroundCell(10, 11, 0, true, false),
          new ExpectedGroundCell(9, 11, 0, true, false),
          new ExpectedGroundCell(8, 11, 0, true, false),
        ]),
      ]),
      new MoveState(299, 4, [
        new TrafficJamState([
          new ExpectedGroundCell(15, 17, 270, true, false),
          new ExpectedGroundCell(15, 16, 270, true, false),
          new ExpectedGroundCell(15, 15, 270, true, false),
          new ExpectedGroundCell(15, 14, 270, true, false),
          new ExpectedGroundCell(15, 13, 270, true, false),
          new ExpectedGroundCell(15, 12, 270, true, false),
          new ExpectedGroundCell(15, 11, 0, false, true),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 17, 270, true, false),
          new ExpectedGroundCell(17, 16, 270, true, false),
          new ExpectedGroundCell(17, 15, 270, true, false),
          new ExpectedGroundCell(17, 14, 270, true, false),
          new ExpectedGroundCell(17, 13, 270, true, false),
          new ExpectedGroundCell(17, 12, 270, true, false),
          new ExpectedGroundCell(17, 11, 0, false, true),
          new ExpectedGroundCell(17, 10, 180, true, false),
          new ExpectedGroundCell(18, 10, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(16, 17, 270, true, false),
          new ExpectedGroundCell(16, 16, 270, true, false),
          new ExpectedGroundCell(16, 15, 270, true, false),
          new ExpectedGroundCell(16, 14, 270, true, false),
          new ExpectedGroundCell(16, 13, 270, true, false),
          new ExpectedGroundCell(16, 12, 270, true, false),
          new ExpectedGroundCell(16, 11, 0, false, true),
          new ExpectedGroundCell(16, 10, 270, true, false),
          new ExpectedGroundCell(16, 9, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(28, 11, 0, true, false),
          new ExpectedGroundCell(27, 11, 0, true, false),
          new ExpectedGroundCell(26, 11, 0, true, false),
          new ExpectedGroundCell(25, 11, 0, true, false),
          new ExpectedGroundCell(24, 11, 0, true, false),
          new ExpectedGroundCell(23, 11, 0, true, false),
          new ExpectedGroundCell(22, 11, 0, true, false),
          new ExpectedGroundCell(21, 11, 0, true, false),
          new ExpectedGroundCell(20, 11, 0, true, false),
          new ExpectedGroundCell(19, 11, 0, true, false),
          new ExpectedGroundCell(18, 11, 0, true, false),
          new ExpectedGroundCell(17, 11, 0, false, true),
          new ExpectedGroundCell(16, 11, 0, false, true),
          new ExpectedGroundCell(15, 11, 0, false, true),
          new ExpectedGroundCell(14, 11, 0, true, false),
          new ExpectedGroundCell(13, 11, 0, true, false),
          new ExpectedGroundCell(12, 11, 0, true, false),
          new ExpectedGroundCell(11, 11, 0, true, false),
          new ExpectedGroundCell(10, 11, 0, true, false),
        ]),
      ]),
      new MoveState(414, 4, [
        new TrafficJamState([
          new ExpectedGroundCell(15, 17, 270, true, false),
          new ExpectedGroundCell(15, 16, 270, true, false),
          new ExpectedGroundCell(15, 15, 270, true, false),
          new ExpectedGroundCell(15, 14, 270, true, false),
          new ExpectedGroundCell(15, 13, 270, true, false),
          new ExpectedGroundCell(15, 12, 270, true, false),
          new ExpectedGroundCell(15, 11, 0, false, true),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 17, 270, true, false),
          new ExpectedGroundCell(17, 16, 270, true, false),
          new ExpectedGroundCell(17, 15, 270, true, false),
          new ExpectedGroundCell(17, 14, 270, true, false),
          new ExpectedGroundCell(17, 13, 270, true, false),
          new ExpectedGroundCell(17, 12, 270, true, false),
          new ExpectedGroundCell(17, 11, 0, false, true),
          new ExpectedGroundCell(17, 10, 180, true, false),
          new ExpectedGroundCell(18, 10, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(16, 17, 270, true, false),
          new ExpectedGroundCell(16, 16, 270, true, false),
          new ExpectedGroundCell(16, 15, 270, true, false),
          new ExpectedGroundCell(16, 14, 270, true, false),
          new ExpectedGroundCell(16, 13, 270, true, false),
          new ExpectedGroundCell(16, 12, 270, true, false),
          new ExpectedGroundCell(16, 11, 0, false, true),
          new ExpectedGroundCell(16, 10, 270, true, false),
          new ExpectedGroundCell(16, 9, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 0, true, false),
          new ExpectedGroundCell(28, 11, 0, true, false),
          new ExpectedGroundCell(27, 11, 0, true, false),
          new ExpectedGroundCell(26, 11, 0, true, false),
          new ExpectedGroundCell(25, 11, 0, true, false),
          new ExpectedGroundCell(24, 11, 0, true, false),
          new ExpectedGroundCell(23, 11, 0, true, false),
          new ExpectedGroundCell(22, 11, 0, true, false),
          new ExpectedGroundCell(21, 11, 0, true, false),
          new ExpectedGroundCell(20, 11, 0, true, false),
          new ExpectedGroundCell(19, 11, 0, true, false),
          new ExpectedGroundCell(18, 11, 0, true, false),
        ]),
      ]),
      new MoveState(536, 4, [
        new TrafficJamState([
          new ExpectedGroundCell(18, 19, 0, true, false),
          new ExpectedGroundCell(17, 19, 0, true, false),
          new ExpectedGroundCell(16, 19, 0, true, false),
          new ExpectedGroundCell(15, 19, 270, true, false),
          new ExpectedGroundCell(15, 18, 270, true, false),
          new ExpectedGroundCell(15, 17, 270, true, false),
          new ExpectedGroundCell(15, 16, 270, true, false),
          new ExpectedGroundCell(15, 15, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(21, 18, 0, true, false),
          new ExpectedGroundCell(20, 18, 0, true, false),
          new ExpectedGroundCell(19, 18, 0, true, false),
          new ExpectedGroundCell(18, 18, 0, true, false),
          new ExpectedGroundCell(17, 18, 270, true, false),
          new ExpectedGroundCell(17, 17, 270, true, false),
          new ExpectedGroundCell(17, 16, 270, true, false),
          new ExpectedGroundCell(17, 15, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(16, 18, 270, true, false),
          new ExpectedGroundCell(16, 17, 270, true, false),
          new ExpectedGroundCell(16, 16, 270, true, false),
          new ExpectedGroundCell(16, 15, 270, true, false),
          new ExpectedGroundCell(16, 14, 270, true, false),
          new ExpectedGroundCell(16, 13, 270, true, false),
          new ExpectedGroundCell(16, 12, 270, true, false),
          new ExpectedGroundCell(16, 11, 0, true, true),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 0, true, false),
          new ExpectedGroundCell(28, 11, 0, true, false),
          new ExpectedGroundCell(27, 11, 0, true, false),
          new ExpectedGroundCell(26, 11, 0, true, false),
          new ExpectedGroundCell(25, 11, 0, true, false),
          new ExpectedGroundCell(24, 11, 0, true, false),
          new ExpectedGroundCell(23, 11, 0, true, false),
          new ExpectedGroundCell(22, 11, 0, true, false),
          new ExpectedGroundCell(21, 11, 0, true, false),
          new ExpectedGroundCell(20, 11, 0, true, false),
          new ExpectedGroundCell(19, 11, 0, true, false),
          new ExpectedGroundCell(18, 11, 0, true, false),
        ]),
      ]),
    ];
  }
}
