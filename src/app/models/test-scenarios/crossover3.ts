import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import { ExpectedGroundCell } from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover3 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 3';
  }

  public getDescription(): string {
    return 'Crossover on trailing cell of jam';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 14, 0, 13],
      [16, 9, 0, 10],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(66, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 8, 90, false, false),
          new ExpectedGroundCell(17, 9, 0, true, true),
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 90, true, false),
          new ExpectedGroundCell(17, 12, 90, true, false),
          new ExpectedGroundCell(17, 13, 90, true, false),
          new ExpectedGroundCell(17, 14, 0, true, false),
          new ExpectedGroundCell(16, 14, 0, true, false),
          new ExpectedGroundCell(15, 14, 0, true, false),
          new ExpectedGroundCell(14, 14, 0, true, false),
          new ExpectedGroundCell(13, 14, 0, true, false),
          new ExpectedGroundCell(12, 14, 0, true, false),
          new ExpectedGroundCell(11, 14, 0, true, false),
          new ExpectedGroundCell(10, 14, 0, true, false),
          new ExpectedGroundCell(9, 14, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, true),
        ]),
      ]),
      new MoveState(234, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 3, 90, true, false),
          new ExpectedGroundCell(17, 4, 90, true, false),
          new ExpectedGroundCell(17, 5, 90, true, false),
          new ExpectedGroundCell(17, 6, 90, true, false),
          new ExpectedGroundCell(17, 7, 90, true, false),
          new ExpectedGroundCell(17, 8, 90, true, false),
          new ExpectedGroundCell(17, 9, 0, true, true),
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 90, true, false),
          new ExpectedGroundCell(17, 12, 90, true, false),
          new ExpectedGroundCell(17, 13, 90, true, false),
          new ExpectedGroundCell(17, 14, 0, true, false),
          new ExpectedGroundCell(16, 14, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
        ]),
      ]),
      new MoveState(518, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(10, 0, 180, true, false),
          new ExpectedGroundCell(11, 0, 180, true, false),
          new ExpectedGroundCell(12, 0, 180, true, false),
          new ExpectedGroundCell(13, 0, 180, true, false),
          new ExpectedGroundCell(14, 0, 180, true, false),
          new ExpectedGroundCell(15, 0, 180, true, false),
          new ExpectedGroundCell(16, 0, 180, true, false),
          new ExpectedGroundCell(17, 0, 90, true, false),
          new ExpectedGroundCell(17, 1, 90, true, false),
          new ExpectedGroundCell(17, 2, 90, true, false),
          new ExpectedGroundCell(17, 3, 90, true, false),
          new ExpectedGroundCell(17, 4, 90, true, false),
          new ExpectedGroundCell(17, 5, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
        ]),
      ]),
    ];
  }
}
