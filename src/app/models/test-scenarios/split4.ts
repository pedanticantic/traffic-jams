import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split4 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 4';
  }

  public getDescription(): string {
    return '4 jams, split 2nd, check order of jams';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 6, 0, 6],
      [4, 10, 0, 6],
      [4, 14, 0, 6],
      [4, 18, 0, 6],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [1, 5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(85, 5, [
        new TrafficJamState([
          new ExpectedGroundCell(15, 6, 0, true, false),
          new ExpectedGroundCell(14, 6, 0, true, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(12, 6, 0, true, false),
          new ExpectedGroundCell(11, 6, 0, true, false),
          new ExpectedGroundCell(10, 6, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(8, 7, 180, true, false),
          new ExpectedGroundCell(9, 7, 180, true, false),
          new ExpectedGroundCell(10, 7, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(12, 13, 0, true, false),
          new ExpectedGroundCell(11, 13, 0, true, false),
          new ExpectedGroundCell(10, 13, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(10, 14, 0, true, false),
          new ExpectedGroundCell(9, 14, 0, true, false),
          new ExpectedGroundCell(8, 14, 0, true, false),
          new ExpectedGroundCell(7, 14, 0, true, false),
          new ExpectedGroundCell(6, 14, 0, true, false),
          new ExpectedGroundCell(5, 14, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(10, 18, 0, true, false),
          new ExpectedGroundCell(9, 18, 0, true, false),
          new ExpectedGroundCell(8, 18, 0, true, false),
          new ExpectedGroundCell(7, 18, 0, true, false),
          new ExpectedGroundCell(6, 18, 0, true, false),
          new ExpectedGroundCell(5, 18, 0, true, false),
        ]),
      ]),
    ];
  }
}
