import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split1 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 1';
  }

  public getDescription(): string {
    return 'Basic splitting';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 10, 0, 16],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(38, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(20, 5, 90, true, false),
          new ExpectedGroundCell(20, 6, 90, true, false),
          new ExpectedGroundCell(20, 7, 90, true, false),
          new ExpectedGroundCell(20, 8, 90, false, false),
          new ExpectedGroundCell(20, 9, 90, false, false),
          new ExpectedGroundCell(20, 10, 0, false, false),
          new ExpectedGroundCell(19, 10, 0, false, false),
          new ExpectedGroundCell(18, 10, 0, false, false),
          new ExpectedGroundCell(17, 10, 0, false, false),
          new ExpectedGroundCell(16, 10, 0, false, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
          new ExpectedGroundCell(11, 10, 0, true, false),
          new ExpectedGroundCell(10, 10, 0, true, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
          new ExpectedGroundCell(7, 10, 0, true, false),
          new ExpectedGroundCell(6, 10, 0, true, false),
          new ExpectedGroundCell(5, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(20, 15, 270, true, false),
          new ExpectedGroundCell(20, 14, 270, true, false),
          new ExpectedGroundCell(20, 13, 270, false, false),
          new ExpectedGroundCell(20, 12, 270, false, false),
          new ExpectedGroundCell(20, 11, 270, false, false),
          new ExpectedGroundCell(20, 10, 0, false, false),
          new ExpectedGroundCell(19, 10, 0, false, false),
          new ExpectedGroundCell(18, 10, 0, false, false),
          new ExpectedGroundCell(17, 10, 0, false, false),
          new ExpectedGroundCell(16, 10, 0, false, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
          new ExpectedGroundCell(11, 10, 0, true, false),
          new ExpectedGroundCell(10, 10, 0, true, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
          new ExpectedGroundCell(7, 10, 0, true, false),
          new ExpectedGroundCell(6, 10, 0, true, false),
          new ExpectedGroundCell(5, 10, 0, true, false),
        ]),
      ]),
      new MoveState(153, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(20, 5, 90, true, false),
          new ExpectedGroundCell(20, 6, 90, true, false),
          new ExpectedGroundCell(20, 7, 90, true, false),
          new ExpectedGroundCell(20, 8, 90, true, false),
          new ExpectedGroundCell(20, 9, 90, true, false),
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
        ], [
          new ExpectedGroundCell(14, 10, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(20, 15, 270, true, false),
          new ExpectedGroundCell(20, 14, 270, true, false),
          new ExpectedGroundCell(20, 13, 270, true, false),
          new ExpectedGroundCell(20, 12, 270, true, false),
          new ExpectedGroundCell(20, 11, 270, true, false),
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
        ], [
          new ExpectedGroundCell(14, 10, 0, true, false),
        ]),
      ]),
      new MoveState(260, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(20, 0, 90, true, false),
          new ExpectedGroundCell(20, 1, 90, true, false),
          new ExpectedGroundCell(20, 2, 90, true, false),
          new ExpectedGroundCell(20, 3, 90, true, false),
          new ExpectedGroundCell(20, 4, 90, true, false),
          new ExpectedGroundCell(20, 5, 90, true, false),
          new ExpectedGroundCell(20, 6, 90, true, false),
          new ExpectedGroundCell(20, 7, 90, true, false),
          new ExpectedGroundCell(20, 8, 90, true, false),
          new ExpectedGroundCell(20, 9, 90, true, false),
        ], [
          new ExpectedGroundCell(20, 10, 0, false, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(21, 19, 0, true, false),
          new ExpectedGroundCell(20, 19, 270, true, false),
          new ExpectedGroundCell(20, 18, 270, true, false),
          new ExpectedGroundCell(20, 17, 270, true, false),
          new ExpectedGroundCell(20, 16, 270, true, false),
          new ExpectedGroundCell(20, 15, 270, true, false),
        ], [
          new ExpectedGroundCell(20, 13, 0, true, false),
          new ExpectedGroundCell(20, 14, 0, true, false),
        ]),
      ]),
    ];
  }
}
