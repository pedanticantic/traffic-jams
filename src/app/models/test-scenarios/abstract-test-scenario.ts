import {MoveState} from "../test-models/move-state";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {Container} from "../container";

export abstract class AbstractTestScenario {
  public abstract getName(): string;
  public abstract getDescription(): string;
  public abstract getJamConfig(): number[][];
  protected abstract makeContainer(): Container;
  protected abstract makeMoveStateAssertions(): MoveState[];
  private readonly container: Container;
  private readonly moveStateAssertions: MoveState[];
  private isRunning: boolean = false;
  public testIndex: number = 0;

  constructor() {
    this.container = this.makeContainer();
    this.moveStateAssertions = this.makeMoveStateAssertions();
  }

  public getDieRollSequence(): number[] {
    return [4, 1];
  }

  public getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [];
  }

  public getSlug(): string {
    // @TODO: This needs to be a lot more sophisticated.
    return this.getName().replaceAll(' ', '_');
  }

  public getContainer(): Container {
    return this.container;
  }

  public getMoveStateAssertions(): MoveState[] {
    return this.moveStateAssertions;
  }

  public setRunning(): void {
    this.isRunning = true;
  }

  public isCurrentlyRunning(): boolean {
    return this.isRunning;
  }
}
