import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {Rectangle} from "../rectangle";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Obstacles3 extends AbstractTestScenario {
  public getName(): string {
    return 'Obstacles 3';
  }

  public getDescription(): string {
    return 'Jam blocked right & forward, but not left, and is told to turn right';
  }

  public getJamConfig(): number[][] {
    return [
      [29, 14, 90, 8],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [3, 1];
  }

  protected makeContainer(): Container {
    return Container.makeStandard()
      .subtractRectangle(new Rectangle(28, 29, 4, 5));
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(50, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(24, 6, 180, true, false),
          new ExpectedGroundCell(25, 6, 180, true, false),
          new ExpectedGroundCell(26, 6, 180, true, false),
          new ExpectedGroundCell(27, 6, 180, true, false),
          new ExpectedGroundCell(28, 6, 180, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false)
        ]),
      ], true),
    ];
  }
}
