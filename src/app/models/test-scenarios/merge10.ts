import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Merge10 extends AbstractTestScenario {
  public getName(): string {
    return 'Merge 10';
  }

  public getDescription(): string {
    return 'Three jams all merging into the tail of another jam';
  }

  public getJamConfig(): number[][] {
    return [
      [11, 1, 270, 6],
      [11, 17, 90, 4],
      [1, 10, 0, 7],
      [10, 10, 0, 10],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  public override getDieRollSequence(): number[] {
    return [6, 6, 6, 1];
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(57, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
          new ExpectedGroundCell(11, 10, 0, true, false),
          new ExpectedGroundCell(11, 9, 270, true, false),
          new ExpectedGroundCell(11, 8, 270, true, false),
          new ExpectedGroundCell(11, 7, 270, true, false),
          new ExpectedGroundCell(11, 6, 270, true, false),
          new ExpectedGroundCell(11, 5, 270, true, false),
          new ExpectedGroundCell(11, 4, 270, true, false),
          new ExpectedGroundCell(11, 11, 90, true, false),
          new ExpectedGroundCell(11, 12, 90, true, false),
          new ExpectedGroundCell(11, 13, 90, true, false),
          new ExpectedGroundCell(11, 14, 90, true, false),
          new ExpectedGroundCell(10, 10, 0, true, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
          new ExpectedGroundCell(7, 10, 0, true, false),
          new ExpectedGroundCell(6, 10, 0, true, false),
          new ExpectedGroundCell(5, 10, 0, true, false),
          new ExpectedGroundCell(4, 10, 0, true, false)
        ])
      ]),
      new MoveState(134, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(25, 10, 0, true, false),
          new ExpectedGroundCell(24, 10, 0, true, false),
          new ExpectedGroundCell(23, 10, 0, true, false),
          new ExpectedGroundCell(22, 10, 0, true, false),
          new ExpectedGroundCell(21, 10, 0, true, false),
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, false, false),
          new ExpectedGroundCell(11, 10, 0, false, false),
          new ExpectedGroundCell(11, 9, 270, false, false),
          new ExpectedGroundCell(11, 8, 270, true, false),
          new ExpectedGroundCell(11, 7, 270, true, false),
          new ExpectedGroundCell(11, 6, 270, true, false),
          new ExpectedGroundCell(11, 5, 270, true, false),
          new ExpectedGroundCell(11, 4, 270, true, false),
          new ExpectedGroundCell(11, 11, 90, false, false),
          new ExpectedGroundCell(11, 12, 90, true, false),
          new ExpectedGroundCell(11, 13, 90, true, false),
          new ExpectedGroundCell(11, 14, 90, true, false),
          new ExpectedGroundCell(10, 10, 0, false, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
          new ExpectedGroundCell(7, 10, 0, true, false),
          new ExpectedGroundCell(6, 10, 0, true, false),
          new ExpectedGroundCell(5, 10, 0, true, false),
          new ExpectedGroundCell(4, 10, 0, true, false)
        ])
      ]),
      new MoveState(305, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 0, true, false),
          new ExpectedGroundCell(28, 10, 0, true, false),
          new ExpectedGroundCell(27, 10, 0, true, false),
          new ExpectedGroundCell(26, 10, 0, true, false),
          new ExpectedGroundCell(25, 10, 0, true, false),
          new ExpectedGroundCell(24, 10, 0, true, false),
          new ExpectedGroundCell(23, 10, 0, true, false),
          new ExpectedGroundCell(22, 10, 0, true, false),
          new ExpectedGroundCell(21, 10, 0, true, false),
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false),
          new ExpectedGroundCell(13, 10, 0, true, false),
          new ExpectedGroundCell(12, 10, 0, true, false),
          new ExpectedGroundCell(11, 10, 0, true, false),
          new ExpectedGroundCell(11, 9, 270, true, false),
          new ExpectedGroundCell(11, 8, 270, true, false),
          new ExpectedGroundCell(11, 11, 90, true, false),
          new ExpectedGroundCell(10, 10, 0, true, false),
          new ExpectedGroundCell(9, 10, 0, true, false),
          new ExpectedGroundCell(8, 10, 0, true, false),
          new ExpectedGroundCell(7, 10, 0, true, false)
        ])
      ]),
      new MoveState(618, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 0, true, false),
          new ExpectedGroundCell(28, 10, 0, true, false),
          new ExpectedGroundCell(27, 10, 0, true, false),
          new ExpectedGroundCell(26, 10, 0, true, false),
          new ExpectedGroundCell(25, 10, 0, true, false),
          new ExpectedGroundCell(24, 10, 0, true, false),
          new ExpectedGroundCell(23, 10, 0, true, false),
          new ExpectedGroundCell(22, 10, 0, true, false),
          new ExpectedGroundCell(21, 10, 0, true, false),
          new ExpectedGroundCell(20, 10, 0, true, false),
          new ExpectedGroundCell(19, 10, 0, true, false),
          new ExpectedGroundCell(18, 10, 0, true, false),
          new ExpectedGroundCell(17, 10, 0, true, false),
          new ExpectedGroundCell(16, 10, 0, true, false),
          new ExpectedGroundCell(15, 10, 0, true, false),
          new ExpectedGroundCell(14, 10, 0, true, false)
        ])
      ]),
    ];
  }
}

/*

 */
