import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import { ExpectedGroundCell } from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover2 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 2';
  }

  public getDescription(): string {
    return 'Crossover on front cell of jam';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 14, 0, 15],
      [7, 9, 0, 12],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(76, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(19, 8, 90, false, false),
          new ExpectedGroundCell(19, 9, 0, true, true),
          new ExpectedGroundCell(19, 10, 90, true, false),
          new ExpectedGroundCell(19, 11, 90, true, false),
          new ExpectedGroundCell(19, 12, 90, true, false),
          new ExpectedGroundCell(19, 13, 90, true, false),
          new ExpectedGroundCell(19, 14, 0, true, false),
          new ExpectedGroundCell(18, 14, 0, true, false),
          new ExpectedGroundCell(17, 14, 0, true, false),
          new ExpectedGroundCell(16, 14, 0, true, false),
          new ExpectedGroundCell(15, 14, 0, true, false),
          new ExpectedGroundCell(14, 14, 0, true, false),
          new ExpectedGroundCell(13, 14, 0, true, false),
          new ExpectedGroundCell(12, 14, 0, true, false),
          new ExpectedGroundCell(11, 14, 0, true, false),
          new ExpectedGroundCell(10, 14, 0, true, false),
          new ExpectedGroundCell(9, 14, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(19, 9, 0, true, true),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, false),
          new ExpectedGroundCell(16, 9, 0, true, false),
          new ExpectedGroundCell(15, 9, 0, true, false),
          new ExpectedGroundCell(14, 9, 0, true, false),
          new ExpectedGroundCell(13, 9, 0, true, false),
          new ExpectedGroundCell(12, 9, 0, true, false),
          new ExpectedGroundCell(11, 9, 0, true, false),
          new ExpectedGroundCell(10, 9, 0, true, false),
          new ExpectedGroundCell(9, 9, 0, true, false),
          new ExpectedGroundCell(8, 9, 0, true, false),
        ]),
      ]),
      new MoveState(432, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 90, true, false),
          new ExpectedGroundCell(19, 1, 90, true, false),
          new ExpectedGroundCell(19, 2, 90, true, false),
          new ExpectedGroundCell(19, 3, 90, true, false),
          new ExpectedGroundCell(19, 4, 90, true, false),
          new ExpectedGroundCell(19, 5, 90, true, false),
          new ExpectedGroundCell(19, 6, 90, true, false),
          new ExpectedGroundCell(19, 7, 90, true, false),
          new ExpectedGroundCell(19, 8, 90, true, false),
          new ExpectedGroundCell(19, 9, 0, false, true),
          new ExpectedGroundCell(19, 10, 90, true, false),
          new ExpectedGroundCell(19, 11, 90, true, false),
          new ExpectedGroundCell(19, 12, 90, true, false),
          new ExpectedGroundCell(19, 13, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, false, true),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, false),
        ]),
      ]),
      new MoveState(508, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 90, true, false),
          new ExpectedGroundCell(19, 1, 90, true, false),
          new ExpectedGroundCell(19, 2, 90, true, false),
          new ExpectedGroundCell(19, 3, 90, true, false),
          new ExpectedGroundCell(19, 4, 90, true, false),
          new ExpectedGroundCell(19, 5, 90, true, false),
          new ExpectedGroundCell(19, 6, 90, true, false),
          new ExpectedGroundCell(19, 7, 90, true, false),
          new ExpectedGroundCell(19, 8, 90, true, false),
          new ExpectedGroundCell(19, 9, 0, false, true),
          new ExpectedGroundCell(19, 10, 90, true, false),
          new ExpectedGroundCell(19, 11, 90, true, false),
          new ExpectedGroundCell(19, 12, 90, true, false),
          new ExpectedGroundCell(19, 13, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
        ]),
      ]),
      new MoveState(604, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(12, 0, 180, true, false),
          new ExpectedGroundCell(13, 0, 180, true, false),
          new ExpectedGroundCell(14, 0, 180, true, false),
          new ExpectedGroundCell(15, 0, 180, true, false),
          new ExpectedGroundCell(16, 0, 180, true, false),
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 90, true, false),
          new ExpectedGroundCell(19, 1, 90, true, false),
          new ExpectedGroundCell(19, 2, 90, true, false),
          new ExpectedGroundCell(19, 3, 90, true, false),
          new ExpectedGroundCell(19, 4, 90, true, false),
          new ExpectedGroundCell(19, 5, 90, true, false),
          new ExpectedGroundCell(19, 6, 90, true, false),
          new ExpectedGroundCell(19, 7, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
        ]),
      ]),
    ];
  }
}
