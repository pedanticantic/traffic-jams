import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover4 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 4';
  }

  public getDescription(): string {
    return '2 crossovers going through 1 jam, non-consecutive cells';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 11, 0, 8],
      [25, 11, 180, 8],
      [7, 9, 0, 16],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [4, 4, 1];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(35, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(12, 8, 90, false, false),
          new ExpectedGroundCell(12, 9, 0, true, true),
          new ExpectedGroundCell(12, 10, 90, true, false),
          new ExpectedGroundCell(12, 11, 0, true, false),
          new ExpectedGroundCell(11, 11, 0, true, false),
          new ExpectedGroundCell(10, 11, 0, true, false),
          new ExpectedGroundCell(9, 11, 0, true, false),
          new ExpectedGroundCell(8, 11, 0, true, false),
          new ExpectedGroundCell(7, 11, 0, true, false),
          new ExpectedGroundCell(6, 11, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 8, 90, false, false),
          new ExpectedGroundCell(17, 9, 0, true, true),
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 180, true, false),
          new ExpectedGroundCell(18, 11, 180, true, false),
          new ExpectedGroundCell(19, 11, 180, true, false),
          new ExpectedGroundCell(20, 11, 180, true, false),
          new ExpectedGroundCell(21, 11, 180, true, false),
          new ExpectedGroundCell(22, 11, 180, true, false),
          new ExpectedGroundCell(23, 11, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, true),
          new ExpectedGroundCell(16, 9, 0, true, false),
          new ExpectedGroundCell(15, 9, 0, true, false),
          new ExpectedGroundCell(14, 9, 0, true, false),
          new ExpectedGroundCell(13, 9, 0, true, false),
          new ExpectedGroundCell(12, 9, 0, true, true),
          new ExpectedGroundCell(11, 9, 0, true, false),
          new ExpectedGroundCell(10, 9, 0, true, false),
          new ExpectedGroundCell(9, 9, 0, true, false),
          new ExpectedGroundCell(8, 9, 0, true, false),
        ]),
      ]),
      new MoveState(359, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(12, 3, 90, true, false),
          new ExpectedGroundCell(12, 4, 90, true, false),
          new ExpectedGroundCell(12, 5, 90, true, false),
          new ExpectedGroundCell(12, 6, 90, true, false),
          new ExpectedGroundCell(12, 7, 90, true, false),
          new ExpectedGroundCell(12, 8, 90, true, false),
          new ExpectedGroundCell(12, 9, 0, false, true),
          new ExpectedGroundCell(12, 10, 90, true, false),
          new ExpectedGroundCell(12, 11, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 3, 90, true, false),
          new ExpectedGroundCell(17, 4, 90, true, false),
          new ExpectedGroundCell(17, 5, 90, true, false),
          new ExpectedGroundCell(17, 6, 90, true, false),
          new ExpectedGroundCell(17, 7, 90, true, false),
          new ExpectedGroundCell(17, 8, 90, true, false),
          new ExpectedGroundCell(17, 9, 0, false, true),
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 180, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
        ]),
      ]),
      new MoveState(463, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(10, 0, 180, true, false),
          new ExpectedGroundCell(11, 0, 180, true, false),
          new ExpectedGroundCell(12, 0, 90, true, false),
          new ExpectedGroundCell(12, 1, 90, true, false),
          new ExpectedGroundCell(12, 2, 90, true, false),
          new ExpectedGroundCell(12, 3, 90, true, false),
          new ExpectedGroundCell(12, 4, 90, true, false),
          new ExpectedGroundCell(12, 5, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(15, 0, 180, true, false),
          new ExpectedGroundCell(16, 0, 180, true, false),
          new ExpectedGroundCell(17, 0, 90, true, false),
          new ExpectedGroundCell(17, 1, 90, true, false),
          new ExpectedGroundCell(17, 2, 90, true, false),
          new ExpectedGroundCell(17, 3, 90, true, false),
          new ExpectedGroundCell(17, 4, 90, true, false),
          new ExpectedGroundCell(17, 5, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
        ]),
      ]),
    ];
  }
}
