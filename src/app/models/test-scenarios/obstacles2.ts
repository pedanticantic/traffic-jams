import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {Rectangle} from "../rectangle";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Obstacles2 extends AbstractTestScenario {
  public getName(): string {
    return 'Obstacles 2';
  }

  public getDescription(): string {
    return 'Make a jam traverse a maze in order to cross over another jam';
  }

  public getJamConfig(): number[][] {
    return [
      [2, 8, 0, 4],
      [22, 4, 270, 8],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [4, 1];
  }

  protected makeContainer(): Container {
    return new Container(new Rectangle(0, 25, 0, 15))
      .subtractRectangle(new Rectangle(8, 18, 0, 15))
      .addRectangle(new Rectangle(9, 9, 1, 14))
      .addRectangle(new Rectangle(10, 17, 1, 1))
      .addRectangle(new Rectangle(17, 17, 2, 6))
      .addRectangle(new Rectangle(8, 8, 8, 8))
      .addRectangle(new Rectangle(10, 16, 14, 14))
      .addRectangle(new Rectangle(16, 16, 12, 13))
      .addRectangle(new Rectangle(17, 17, 10, 12))
      .addRectangle(new Rectangle(11, 17, 12, 12))
      .addRectangle(new Rectangle(11, 11, 3, 11))
      .addRectangle(new Rectangle(13, 13, 5, 11))
      .addRectangle(new Rectangle(12, 15, 3, 3))
      .addRectangle(new Rectangle(15, 15, 4, 10))
      .addRectangle(new Rectangle(15, 18, 8, 8));
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(97, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(23, 8, 0, false, false),
          new ExpectedGroundCell(22, 8, 270, true, true),
          new ExpectedGroundCell(21, 8, 0, true, false),
          new ExpectedGroundCell(20, 8, 0, true, false),
          new ExpectedGroundCell(19, 8, 0, false, false),
          new ExpectedGroundCell(18, 8, 0, false, false),
          new ExpectedGroundCell(17, 8, 0, false, false),
          new ExpectedGroundCell(16, 8, 0, false, false),
          new ExpectedGroundCell(15, 8, 270, false, false),
          new ExpectedGroundCell(15, 7, 270, false, false),
          new ExpectedGroundCell(15, 6, 270, false, false),
          new ExpectedGroundCell(15, 5, 270, false, false),
          new ExpectedGroundCell(15, 4, 270, false, false),
          new ExpectedGroundCell(15, 3, 0, false, false),
          new ExpectedGroundCell(14, 3, 0, false, false),
          new ExpectedGroundCell(13, 3, 0, false, false),
          new ExpectedGroundCell(12, 3, 0, false, false),
          new ExpectedGroundCell(11, 3, 90, false, false),
          new ExpectedGroundCell(11, 4, 90, false, false),
          new ExpectedGroundCell(11, 5, 90, false, false),
          new ExpectedGroundCell(11, 6, 90, false, false),
          new ExpectedGroundCell(11, 7, 90, false, false),
          new ExpectedGroundCell(11, 8, 90, false, false),
          new ExpectedGroundCell(11, 9, 90, false, false),
          new ExpectedGroundCell(11, 10, 90, false, false),
          new ExpectedGroundCell(11, 11, 90, false, false),
          new ExpectedGroundCell(11, 12, 180, false, false),
          new ExpectedGroundCell(12, 12, 180, false, false),
          new ExpectedGroundCell(13, 12, 180, false, false),
          new ExpectedGroundCell(14, 12, 180, false, false),
          new ExpectedGroundCell(15, 12, 180, false, false),
          new ExpectedGroundCell(16, 12, 90, false, false),
          new ExpectedGroundCell(16, 13, 90, false, false),
          new ExpectedGroundCell(16, 14, 0, false, false),
          new ExpectedGroundCell(15, 14, 0, false, false),
          new ExpectedGroundCell(14, 14, 0, false, false),
          new ExpectedGroundCell(13, 14, 0, false, false),
          new ExpectedGroundCell(12, 14, 0, false, false),
          new ExpectedGroundCell(11, 14, 0, false, false),
          new ExpectedGroundCell(10, 14, 0, false, false),
          new ExpectedGroundCell(9, 14, 270, false, false),
          new ExpectedGroundCell(9, 13, 270, false, false),
          new ExpectedGroundCell(9, 12, 270, false, false),
          new ExpectedGroundCell(9, 11, 270, false, false),
          new ExpectedGroundCell(9, 10, 270, false, false),
          new ExpectedGroundCell(9, 9, 270, false, false),
          new ExpectedGroundCell(9, 8, 0, false, false),
          new ExpectedGroundCell(8, 8, 0, false, false),
          new ExpectedGroundCell(7, 8, 0, false, false),
          new ExpectedGroundCell(6, 8, 0, false, false),
          new ExpectedGroundCell(5, 8, 0, false, false),
          new ExpectedGroundCell(4, 8, 0, true, false),
          new ExpectedGroundCell(3, 8, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(22, 12, 270, true, false),
          new ExpectedGroundCell(22, 11, 270, true, false),
          new ExpectedGroundCell(22, 10, 270, true, false),
          new ExpectedGroundCell(22, 9, 270, true, false),
          new ExpectedGroundCell(22, 8, 270, true, true),
          new ExpectedGroundCell(22, 7, 270, true, false),
          new ExpectedGroundCell(22, 6, 270, true, false),
          new ExpectedGroundCell(22, 5, 270, true, false),
        ]),
      ]),
    ];
  }
}
