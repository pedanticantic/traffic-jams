import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Merge9 extends AbstractTestScenario {
  public getName(): string {
    return 'Merge 9';
  }

  public getDescription(): string {
    return 'Two jams merging into another jam at the same point';
  }

  public getJamConfig(): number[][] {
    return [
      [3, 6, 0, 10],
      [3, 18, 0, 10],
      [6, 12, 0, 12],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  public override getDieRollSequence(): number[] {
    return [6, 6, 1];
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(176, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(19, 12, 0, true, false),
          new ExpectedGroundCell(18, 12, 0, true, false),
          new ExpectedGroundCell(17, 12, 0, true, false),
          new ExpectedGroundCell(16, 12, 0, true, false),
          new ExpectedGroundCell(15, 12, 0, true, false),
          new ExpectedGroundCell(14, 12, 0, false, false),
          new ExpectedGroundCell(13, 12, 0, false, false),
          new ExpectedGroundCell(12, 12, 0, false, false),
          new ExpectedGroundCell(11, 12, 0, true, false),
          new ExpectedGroundCell(10, 12, 0, true, false),
          new ExpectedGroundCell(9, 12, 0, true, false),
          new ExpectedGroundCell(8, 12, 0, true, false),
          new ExpectedGroundCell(7, 12, 0, true, false),
          new ExpectedGroundCell(13, 11, 270, false, false),
          new ExpectedGroundCell(13, 10, 270, true, false),
          new ExpectedGroundCell(13, 9, 270, true, false),
          new ExpectedGroundCell(13, 8, 270, true, false),
          new ExpectedGroundCell(13, 7, 270, true, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(12, 6, 0, true, false),
          new ExpectedGroundCell(11, 6, 0, true, false),
          new ExpectedGroundCell(10, 6, 0, true, false),
          new ExpectedGroundCell(9, 6, 0, true, false),
          new ExpectedGroundCell(13, 13, 90, false, false),
          new ExpectedGroundCell(13, 14, 90, true, false),
          new ExpectedGroundCell(13, 15, 90, true, false),
          new ExpectedGroundCell(13, 16, 90, true, false),
          new ExpectedGroundCell(13, 17, 90, true, false),
          new ExpectedGroundCell(13, 18, 0, true, false),
          new ExpectedGroundCell(12, 18, 0, true, false),
          new ExpectedGroundCell(11, 18, 0, true, false),
          new ExpectedGroundCell(10, 18, 0, true, false),
          new ExpectedGroundCell(9, 18, 0, true, false)
        ]),
      ]),
      new MoveState(209, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(19, 12, 0, true, false),
          new ExpectedGroundCell(18, 12, 0, true, false),
          new ExpectedGroundCell(17, 12, 0, true, false),
          new ExpectedGroundCell(16, 12, 0, true, false),
          new ExpectedGroundCell(15, 12, 0, true, false),
          new ExpectedGroundCell(14, 12, 0, true, false),
          new ExpectedGroundCell(13, 12, 0, true, false),
          new ExpectedGroundCell(12, 12, 0, true, false),
          new ExpectedGroundCell(11, 12, 0, true, false),
          new ExpectedGroundCell(10, 12, 0, true, false),
          new ExpectedGroundCell(9, 12, 0, false, false),
          new ExpectedGroundCell(8, 12, 0, false, false),
          new ExpectedGroundCell(7, 12, 0, true, false),
          new ExpectedGroundCell(13, 11, 270, true, false),
          new ExpectedGroundCell(13, 10, 270, true, false),
          new ExpectedGroundCell(13, 9, 270, true, false),
          new ExpectedGroundCell(13, 8, 270, true, false),
          new ExpectedGroundCell(13, 7, 270, false, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(12, 6, 0, true, false),
          new ExpectedGroundCell(11, 6, 0, true, false),
          new ExpectedGroundCell(10, 6, 0, true, false),
          new ExpectedGroundCell(9, 6, 0, true, false),
          new ExpectedGroundCell(13, 13, 90, true, false),
          new ExpectedGroundCell(13, 14, 90, true, false),
          new ExpectedGroundCell(13, 15, 90, true, false),
          new ExpectedGroundCell(13, 16, 90, false, false),
          new ExpectedGroundCell(13, 17, 90, false, false),
          new ExpectedGroundCell(13, 18, 0, true, false),
          new ExpectedGroundCell(12, 18, 0, true, false),
          new ExpectedGroundCell(11, 18, 0, true, false),
          new ExpectedGroundCell(10, 18, 0, true, false),
          new ExpectedGroundCell(9, 18, 0, true, false)
        ]),
      ]),
      new MoveState(652, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 90, true, false),
          new ExpectedGroundCell(29, 12, 0, true, false),
          new ExpectedGroundCell(28, 12, 0, true, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, true, false),
          new ExpectedGroundCell(24, 12, 0, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(19, 12, 0, true, false),
          new ExpectedGroundCell(18, 12, 0, true, false),
          new ExpectedGroundCell(17, 12, 0, true, false),
          new ExpectedGroundCell(16, 12, 0, true, false),
          new ExpectedGroundCell(15, 12, 0, false, false),
          new ExpectedGroundCell(14, 12, 0, false, false),
          new ExpectedGroundCell(13, 12, 0, false, false),
          new ExpectedGroundCell(13, 11, 270, false, false),
          new ExpectedGroundCell(13, 10, 270, true, false),
          new ExpectedGroundCell(13, 9, 270, true, false),
          new ExpectedGroundCell(13, 8, 270, true, false),
          new ExpectedGroundCell(13, 7, 270, true, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(12, 6, 0, true, false),
          new ExpectedGroundCell(13, 13, 90, false, false),
          new ExpectedGroundCell(13, 14, 90, true, false),
          new ExpectedGroundCell(13, 15, 90, true, false),
          new ExpectedGroundCell(13, 16, 90, true, false)
        ]),
      ]),
      new MoveState(1068, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(22, 0, 180, true, false),
          new ExpectedGroundCell(23, 0, 180, true, false),
          new ExpectedGroundCell(24, 0, 180, true, false),
          new ExpectedGroundCell(25, 0, 180, true, false),
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 90, true, false),
          new ExpectedGroundCell(29, 12, 0, true, false),
          new ExpectedGroundCell(28, 12, 0, true, false),
          new ExpectedGroundCell(27, 12, 0, true, false),
          new ExpectedGroundCell(26, 12, 0, true, false),
          new ExpectedGroundCell(25, 12, 0, true, false),
          new ExpectedGroundCell(24, 12, 0, true, false),
          new ExpectedGroundCell(23, 12, 0, true, false),
          new ExpectedGroundCell(22, 12, 0, true, false),
          new ExpectedGroundCell(21, 12, 0, true, false),
          new ExpectedGroundCell(20, 12, 0, true, false),
          new ExpectedGroundCell(19, 12, 0, true, false),
          new ExpectedGroundCell(18, 12, 0, true, false),
          new ExpectedGroundCell(17, 12, 0, true, false)
        ]),
      ]),
    ];
  }
}
