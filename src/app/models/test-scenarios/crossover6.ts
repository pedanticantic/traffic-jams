import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover6 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 6';
  }

  public getDescription(): string {
    return 'Edge crossover';
  }

  public getJamConfig(): number[][] {
    return [
      [15, 8, 270, 8],
      [7, 19, 0, 11],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(69, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(15, 16, 270, true, false),
          new ExpectedGroundCell(15, 15, 270, true, false),
          new ExpectedGroundCell(15, 14, 270, true, false),
          new ExpectedGroundCell(15, 13, 270, true, false),
          new ExpectedGroundCell(15, 12, 270, true, false),
          new ExpectedGroundCell(15, 11, 270, true, false),
          new ExpectedGroundCell(15, 10, 270, true, false),
          new ExpectedGroundCell(15, 9, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(23, 19, 0, true, false),
          new ExpectedGroundCell(22, 19, 0, true, false),
          new ExpectedGroundCell(21, 19, 0, true, false),
          new ExpectedGroundCell(20, 19, 0, true, false),
          new ExpectedGroundCell(19, 19, 0, true, false),
          new ExpectedGroundCell(18, 19, 0, true, false),
          new ExpectedGroundCell(17, 19, 0, true, false),
          new ExpectedGroundCell(16, 19, 0, true, false),
          new ExpectedGroundCell(15, 19, 0, true, false),
          new ExpectedGroundCell(14, 19, 0, true, false),
          new ExpectedGroundCell(13, 19, 0, true, false),
        ]),
      ]),
      new MoveState(119, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(18, 18, 0, true, false),
          new ExpectedGroundCell(17, 18, 0, true, false),
          new ExpectedGroundCell(16, 18, 0, true, false),
          new ExpectedGroundCell(15, 18, 270, true, false),
          new ExpectedGroundCell(15, 17, 270, true, false),
          new ExpectedGroundCell(15, 16, 270, true, false),
          new ExpectedGroundCell(15, 15, 270, true, false),
          new ExpectedGroundCell(15, 14, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(23, 19, 0, true, false),
          new ExpectedGroundCell(22, 19, 0, true, false),
          new ExpectedGroundCell(21, 19, 0, true, false),
          new ExpectedGroundCell(20, 19, 0, true, false),
          new ExpectedGroundCell(19, 19, 0, true, false),
          new ExpectedGroundCell(18, 19, 0, true, false),
          new ExpectedGroundCell(17, 19, 0, true, false),
          new ExpectedGroundCell(16, 19, 0, true, false),
          new ExpectedGroundCell(15, 19, 0, true, false),
          new ExpectedGroundCell(14, 19, 0, true, false),
          new ExpectedGroundCell(13, 19, 0, true, false),
        ]),
      ]),
    ];
  }
}
