import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import { ExpectedGroundCell } from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split5 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 5';
  }

  public getDescription(): string {
    return 'Jam can has to use the 2nd split options';
  }

  public getJamConfig(): number[][] {
    return [
      [16, 2, 0, 12],
      [17, 3, 0, 12],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeForwards()),
        new SplitDirectionDeltaPair(DirectionDelta.makeForwards(), DirectionDelta.makeAntiClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(108, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 0, true, false),
          new ExpectedGroundCell(28, 2, 0, true, false),
          new ExpectedGroundCell(27, 2, 0, true, false),
        ], [
          new ExpectedGroundCell(26, 2, 0, false, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(24, 1, 180, true, false),
          new ExpectedGroundCell(25, 1, 180, true, false),
          new ExpectedGroundCell(26, 1, 180, true, false),
          new ExpectedGroundCell(27, 1, 180, true, false),
          new ExpectedGroundCell(28, 1, 90, true, false),
          new ExpectedGroundCell(28, 2, 0, true, false),
          new ExpectedGroundCell(27, 2, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(29, 3, 0, true, false),
          new ExpectedGroundCell(28, 3, 0, true, false),
          new ExpectedGroundCell(27, 3, 0, true, false),
          new ExpectedGroundCell(26, 3, 0, true, false),
          new ExpectedGroundCell(25, 3, 0, true, false),
          new ExpectedGroundCell(24, 3, 0, true, false),
          new ExpectedGroundCell(23, 3, 0, true, false),
          new ExpectedGroundCell(22, 3, 0, true, false),
          new ExpectedGroundCell(21, 3, 0, true, false),
          new ExpectedGroundCell(20, 3, 0, true, false),
          new ExpectedGroundCell(19, 3, 0, true, false),
          new ExpectedGroundCell(18, 3, 0, true, false),
        ]),
      ]),
    ];
  }
}
