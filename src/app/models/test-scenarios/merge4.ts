import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {Rectangle} from "../rectangle";

export class Merge4 extends AbstractTestScenario {
  public getName(): string {
    return 'Merge 4';
  }

  public getDescription(): string {
    return 'Split a jam, merge one branch into another jam';
  }

  public getJamConfig(): number[][] {
    return [
      [0, 14, 0, 28],
      [5, 6, 0, 22],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard()
      .subtractRectangle(new Rectangle(29, 29, 16, 19));
  }

  public override getDieRollSequence(): number[] {
    return [5, 1, 6, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeForwards(), DirectionDelta.makeClockwise()),
      ],
    ];
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(761, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(23, 19, 180, true, false),
          new ExpectedGroundCell(24, 19, 180, true, false),
          new ExpectedGroundCell(25, 19, 180, true, false),
          new ExpectedGroundCell(26, 19, 180, true, false),
          new ExpectedGroundCell(27, 19, 180, true, false),
          new ExpectedGroundCell(28, 19, 270, true, false),
          new ExpectedGroundCell(28, 18, 270, true, false),
          new ExpectedGroundCell(28, 17, 270, true, false),
          new ExpectedGroundCell(28, 16, 270, true, false),
          new ExpectedGroundCell(28, 15, 270, true, false),
          new ExpectedGroundCell(28, 14, 0, true, false),
          new ExpectedGroundCell(27, 14, 0, true, false),
          new ExpectedGroundCell(26, 14, 0, true, false),
          new ExpectedGroundCell(25, 14, 0, false, false),
          new ExpectedGroundCell(24, 14, 0, false, false),
          new ExpectedGroundCell(23, 14, 0, false, false),
          new ExpectedGroundCell(22, 14, 0, true, false),
          new ExpectedGroundCell(21, 14, 0, true, false),
          new ExpectedGroundCell(20, 14, 0, true, false),
          new ExpectedGroundCell(19, 14, 0, true, false)
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 0, true, false),
          new ExpectedGroundCell(28, 6, 0, true, false),
          new ExpectedGroundCell(27, 6, 0, true, false),
          new ExpectedGroundCell(26, 6, 0, true, false),
          new ExpectedGroundCell(25, 6, 0, true, false),
          new ExpectedGroundCell(24, 6, 0, true, false),
          new ExpectedGroundCell(23, 6, 0, true, false),
          new ExpectedGroundCell(22, 6, 0, true, false),
          new ExpectedGroundCell(21, 6, 0, true, false),
          new ExpectedGroundCell(20, 6, 0, true, false),
          new ExpectedGroundCell(19, 6, 0, true, false),
          new ExpectedGroundCell(18, 6, 0, true, false),
          new ExpectedGroundCell(17, 6, 0, false, false),
          new ExpectedGroundCell(16, 6, 0, false, false),
          new ExpectedGroundCell(15, 6, 0, true, false),
          new ExpectedGroundCell(14, 6, 0, true, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(12, 6, 0, true, false),
          new ExpectedGroundCell(11, 6, 0, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 90, true, false),
          new ExpectedGroundCell(29, 12, 90, true, false),
          new ExpectedGroundCell(29, 13, 90, true, false),
          new ExpectedGroundCell(29, 14, 0, true, false),
          new ExpectedGroundCell(28, 14, 0, true, false),
          new ExpectedGroundCell(27, 14, 0, true, false),
          new ExpectedGroundCell(26, 14, 0, true, false),
          new ExpectedGroundCell(25, 14, 0, false, false),
          new ExpectedGroundCell(24, 14, 0, false, false),
          new ExpectedGroundCell(23, 14, 0, false, false),
          new ExpectedGroundCell(22, 14, 0, true, false),
          new ExpectedGroundCell(21, 14, 0, true, false),
          new ExpectedGroundCell(20, 14, 0, true, false),
          new ExpectedGroundCell(19, 14, 0, true, false)
        ]),
      ]),
      new MoveState(1040, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(18, 19, 180, true, false),
          new ExpectedGroundCell(19, 19, 180, true, false),
          new ExpectedGroundCell(20, 19, 180, true, false),
          new ExpectedGroundCell(21, 19, 180, true, false),
          new ExpectedGroundCell(22, 19, 180, true, false),
          new ExpectedGroundCell(23, 19, 180, true, false),
          new ExpectedGroundCell(24, 19, 180, true, false),
          new ExpectedGroundCell(25, 19, 180, true, false),
          new ExpectedGroundCell(26, 19, 180, true, false),
          new ExpectedGroundCell(27, 19, 180, true, false),
          new ExpectedGroundCell(28, 19, 270, true, false),
          new ExpectedGroundCell(28, 18, 270, true, false),
          new ExpectedGroundCell(28, 17, 270, true, false),
          new ExpectedGroundCell(28, 16, 270, true, false),
          new ExpectedGroundCell(28, 15, 270, true, false)
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(22, 0, 180, true, false),
          new ExpectedGroundCell(23, 0, 180, true, false),
          new ExpectedGroundCell(24, 0, 180, true, false),
          new ExpectedGroundCell(25, 0, 180, true, false),
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 0, true, false),
          new ExpectedGroundCell(28, 6, 0, true, false),
          new ExpectedGroundCell(27, 6, 0, true, false),
          new ExpectedGroundCell(26, 6, 0, true, false),
          new ExpectedGroundCell(25, 6, 0, true, false),
          new ExpectedGroundCell(24, 6, 0, true, false),
          new ExpectedGroundCell(23, 6, 0, true, false),
          new ExpectedGroundCell(22, 6, 0, true, false),
          new ExpectedGroundCell(21, 6, 0, true, false),
          new ExpectedGroundCell(20, 6, 0, true, false),
          new ExpectedGroundCell(19, 6, 0, true, false),
          new ExpectedGroundCell(18, 6, 0, true, false),
          new ExpectedGroundCell(17, 6, 0, true, false),
          new ExpectedGroundCell(16, 6, 0, false, false),
          new ExpectedGroundCell(15, 6, 0, false, false),
          new ExpectedGroundCell(14, 6, 0, true, false),
          new ExpectedGroundCell(13, 6, 0, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, true, false),
          new ExpectedGroundCell(29, 10, 90, true, false),
          new ExpectedGroundCell(29, 11, 90, true, false),
          new ExpectedGroundCell(29, 12, 90, true, false),
          new ExpectedGroundCell(29, 13, 90, true, false)
        ]),
      ]),
      new MoveState(1265, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(13, 19, 180, true, false),
          new ExpectedGroundCell(14, 19, 180, true, false),
          new ExpectedGroundCell(15, 19, 180, true, false),
          new ExpectedGroundCell(16, 19, 180, true, false),
          new ExpectedGroundCell(17, 19, 180, true, false),
          new ExpectedGroundCell(18, 19, 180, true, false),
          new ExpectedGroundCell(19, 19, 180, true, false),
          new ExpectedGroundCell(20, 19, 180, true, false),
          new ExpectedGroundCell(21, 19, 180, true, false),
          new ExpectedGroundCell(22, 19, 180, true, false),
          new ExpectedGroundCell(23, 19, 180, true, false),
          new ExpectedGroundCell(24, 19, 180, true, false),
          new ExpectedGroundCell(25, 19, 180, true, false),
          new ExpectedGroundCell(26, 19, 180, true, false),
          new ExpectedGroundCell(27, 19, 180, true, false)
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 180, true, false),
          new ExpectedGroundCell(20, 0, 180, true, false),
          new ExpectedGroundCell(21, 0, 180, true, false),
          new ExpectedGroundCell(22, 0, 180, true, false),
          new ExpectedGroundCell(23, 0, 180, true, false),
          new ExpectedGroundCell(24, 0, 180, true, false),
          new ExpectedGroundCell(25, 0, 180, true, false),
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false),
          new ExpectedGroundCell(29, 3, 90, true, false),
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 0, true, false),
          new ExpectedGroundCell(28, 6, 0, true, false),
          new ExpectedGroundCell(27, 6, 0, true, false),
          new ExpectedGroundCell(26, 6, 0, true, false),
          new ExpectedGroundCell(25, 6, 0, false, false),
          new ExpectedGroundCell(24, 6, 0, false, false),
          new ExpectedGroundCell(23, 6, 0, true, false),
          new ExpectedGroundCell(22, 6, 0, true, false),
          new ExpectedGroundCell(21, 6, 0, true, false),
          new ExpectedGroundCell(20, 6, 0, true, false),
          new ExpectedGroundCell(19, 6, 0, true, false),
          new ExpectedGroundCell(18, 6, 0, true, false),
          new ExpectedGroundCell(17, 6, 0, true, false),
          new ExpectedGroundCell(16, 6, 0, true, false),
          new ExpectedGroundCell(15, 6, 0, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 90, false, false),
          new ExpectedGroundCell(29, 10, 90, false, false),
          new ExpectedGroundCell(29, 11, 90, false, false),
          new ExpectedGroundCell(29, 12, 90, true, false),
          new ExpectedGroundCell(29, 13, 90, true, false)
        ]),
      ]),
      new MoveState(2468, 2, [
        new TrafficJamState([
          new ExpectedGroundCell(0, 12, 90, true, false),
          new ExpectedGroundCell(0, 13, 90, true, false),
          new ExpectedGroundCell(0, 14, 90, true, false),
          new ExpectedGroundCell(0, 15, 90, true, false),
          new ExpectedGroundCell(0, 16, 90, true, false),
          new ExpectedGroundCell(0, 17, 90, true, false),
          new ExpectedGroundCell(0, 18, 90, true, false),
          new ExpectedGroundCell(0, 19, 180, true, false),
          new ExpectedGroundCell(1, 19, 180, true, false),
          new ExpectedGroundCell(2, 19, 180, true, false),
          new ExpectedGroundCell(3, 19, 180, true, false),
          new ExpectedGroundCell(4, 19, 180, true, false),
          new ExpectedGroundCell(5, 19, 180, true, false),
          new ExpectedGroundCell(6, 19, 180, true, false),
          new ExpectedGroundCell(7, 19, 180, true, false)
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(0, 3, 270, true, false),
          new ExpectedGroundCell(0, 2, 270, true, false),
          new ExpectedGroundCell(0, 1, 270, true, false),
          new ExpectedGroundCell(0, 0, 180, true, false),
          new ExpectedGroundCell(1, 0, 180, true, false),
          new ExpectedGroundCell(2, 0, 180, true, false),
          new ExpectedGroundCell(3, 0, 180, true, false),
          new ExpectedGroundCell(4, 0, 180, true, false),
          new ExpectedGroundCell(5, 0, 180, true, false),
          new ExpectedGroundCell(6, 0, 180, true, false),
          new ExpectedGroundCell(7, 0, 180, true, false),
          new ExpectedGroundCell(8, 0, 180, true, false),
          new ExpectedGroundCell(9, 0, 180, true, false),
          new ExpectedGroundCell(10, 0, 180, true, false),
          new ExpectedGroundCell(11, 0, 180, true, false),
          new ExpectedGroundCell(12, 0, 180, true, false),
          new ExpectedGroundCell(13, 0, 180, true, false),
          new ExpectedGroundCell(14, 0, 180, true, false),
          new ExpectedGroundCell(15, 0, 180, true, false),
          new ExpectedGroundCell(16, 0, 180, true, false),
          new ExpectedGroundCell(17, 0, 180, true, false),
          new ExpectedGroundCell(18, 0, 180, true, false),
          new ExpectedGroundCell(19, 0, 180, true, false),
          new ExpectedGroundCell(20, 0, 180, true, false),
          new ExpectedGroundCell(21, 0, 180, true, false),
          new ExpectedGroundCell(22, 0, 180, true, false),
          new ExpectedGroundCell(23, 0, 180, true, false),
          new ExpectedGroundCell(24, 0, 180, true, false),
          new ExpectedGroundCell(25, 0, 180, true, false),
          new ExpectedGroundCell(26, 0, 180, true, false),
          new ExpectedGroundCell(27, 0, 180, true, false),
          new ExpectedGroundCell(28, 0, 180, true, false),
          new ExpectedGroundCell(29, 0, 90, true, false),
          new ExpectedGroundCell(29, 1, 90, true, false),
          new ExpectedGroundCell(29, 2, 90, true, false)
        ]),
      ]),
    ];
  }
}
