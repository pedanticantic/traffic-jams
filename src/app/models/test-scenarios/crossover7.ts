import {AbstractTestScenario} from "./abstract-test-scenario";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import { ExpectedGroundCell } from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Crossover7 extends AbstractTestScenario {
  public getName(): string {
    return 'Crossover 7';
  }

  public getDescription(): string {
    return 'Make sure it clears the crossover flag';
  }

  public getJamConfig(): number[][] {
    return [
      [20, 10, 270, 2],
      [20, 3, 270, 2],
      [14, 15, 0, 9],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(7, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(20, 16, 270, false, false),
          new ExpectedGroundCell(20, 15, 0, true, true),
          new ExpectedGroundCell(20, 14, 270, true, false),
          new ExpectedGroundCell(20, 13, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(20, 5, 270, true, false),
          new ExpectedGroundCell(20, 4, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(23, 15, 0, true, false),
          new ExpectedGroundCell(22, 15, 0, true, false),
          new ExpectedGroundCell(21, 15, 0, true, false),
          new ExpectedGroundCell(20, 15, 0, true, true),
          new ExpectedGroundCell(19, 15, 0, true, false),
          new ExpectedGroundCell(18, 15, 0, true, false),
          new ExpectedGroundCell(17, 15, 0, true, false),
          new ExpectedGroundCell(16, 15, 0, true, false),
          new ExpectedGroundCell(15, 15, 0, true, false),
        ]),
      ]),
      new MoveState(95, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(22, 19, 0, true, false),
          new ExpectedGroundCell(21, 19, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(20, 10, 270, true, false),
          new ExpectedGroundCell(20, 9, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(28, 15, 0, true, false),
          new ExpectedGroundCell(27, 15, 0, true, false),
          new ExpectedGroundCell(26, 15, 0, true, false),
          new ExpectedGroundCell(25, 15, 0, true, false),
          new ExpectedGroundCell(24, 15, 0, true, false),
          new ExpectedGroundCell(23, 15, 0, true, false),
          new ExpectedGroundCell(22, 15, 0, true, false),
          new ExpectedGroundCell(21, 15, 0, true, false),
          new ExpectedGroundCell(20, 15, 0, false, true),
          new ExpectedGroundCell(19, 15, 0, true, false),
        ]),
      ]),
      new MoveState(108, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(22, 19, 0, true, false),
          new ExpectedGroundCell(21, 19, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(21, 14, 0, true, false),
          new ExpectedGroundCell(20, 14, 270, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(28, 15, 0, true, false),
          new ExpectedGroundCell(27, 15, 0, true, false),
          new ExpectedGroundCell(26, 15, 0, true, false),
          new ExpectedGroundCell(25, 15, 0, true, false),
          new ExpectedGroundCell(24, 15, 0, true, false),
          new ExpectedGroundCell(23, 15, 0, true, false),
          new ExpectedGroundCell(22, 15, 0, true, false),
          new ExpectedGroundCell(21, 15, 0, true, false),
          new ExpectedGroundCell(20, 15, 0, false, true),
          new ExpectedGroundCell(19, 15, 0, true, false),
        ]),
      ]),
    ];
  }
}
