import {AbstractTestScenario} from "./abstract-test-scenario";
import {Container} from "../container";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";

export class Merge1 extends AbstractTestScenario {
  public getName(): string {
    return 'Merge 1';
  }

  public getDescription(): string {
    return 'Merge in middle of jam';
  }

  public getJamConfig(): number[][] {
    return [
      [4, 14, 0, 9],
      [7, 9, 0, 12],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  public override getDieRollSequence(): number[] {
    return [6, 1];
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(110, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, false),
          new ExpectedGroundCell(16, 9, 0, true, false),
          new ExpectedGroundCell(15, 9, 0, true, false),
          new ExpectedGroundCell(14, 9, 0, true, false),
          new ExpectedGroundCell(13, 9, 0, false, false),
          new ExpectedGroundCell(12, 9, 0, false, false),
          new ExpectedGroundCell(11, 9, 0, false, false),
          new ExpectedGroundCell(10, 9, 0, true, false),
          new ExpectedGroundCell(9, 9, 0, true, false),
          new ExpectedGroundCell(8, 9, 0, true, false),
          new ExpectedGroundCell(13, 10, 90, false, false),
          new ExpectedGroundCell(13, 11, 90, false, false),
          new ExpectedGroundCell(13, 12, 90, true, false),
          new ExpectedGroundCell(13, 13, 90, true, false),
          new ExpectedGroundCell(13, 14, 0, true, false),
          new ExpectedGroundCell(12, 14, 0, true, false),
          new ExpectedGroundCell(11, 14, 0, true, false),
          new ExpectedGroundCell(10, 14, 0, true, false),
          new ExpectedGroundCell(9, 14, 0, true, false)
        ]),
      ]),
      new MoveState(232, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, false),
          new ExpectedGroundCell(16, 9, 0, true, false),
          new ExpectedGroundCell(15, 9, 0, false, false),
          new ExpectedGroundCell(14, 9, 0, false, false),
          new ExpectedGroundCell(13, 9, 0, false, false),
          new ExpectedGroundCell(12, 9, 0, false, false),
          new ExpectedGroundCell(11, 9, 0, true, false),
          new ExpectedGroundCell(10, 9, 0, true, false),
          new ExpectedGroundCell(13, 10, 90, false, false),
          new ExpectedGroundCell(13, 11, 90, true, false),
          new ExpectedGroundCell(13, 12, 90, true, false),
          new ExpectedGroundCell(13, 13, 90, true, false),
          new ExpectedGroundCell(13, 14, 0, true, false),
          new ExpectedGroundCell(12, 14, 0, true, false)
        ]),
      ]),
      new MoveState(385, 1, [
        new TrafficJamState([
          new ExpectedGroundCell(29, 4, 90, true, false),
          new ExpectedGroundCell(29, 5, 90, true, false),
          new ExpectedGroundCell(29, 6, 90, true, false),
          new ExpectedGroundCell(29, 7, 90, true, false),
          new ExpectedGroundCell(29, 8, 90, true, false),
          new ExpectedGroundCell(29, 9, 0, true, false),
          new ExpectedGroundCell(28, 9, 0, true, false),
          new ExpectedGroundCell(27, 9, 0, true, false),
          new ExpectedGroundCell(26, 9, 0, true, false),
          new ExpectedGroundCell(25, 9, 0, true, false),
          new ExpectedGroundCell(24, 9, 0, true, false),
          new ExpectedGroundCell(23, 9, 0, true, false),
          new ExpectedGroundCell(22, 9, 0, true, false),
          new ExpectedGroundCell(21, 9, 0, true, false),
          new ExpectedGroundCell(20, 9, 0, true, false),
          new ExpectedGroundCell(19, 9, 0, true, false),
          new ExpectedGroundCell(18, 9, 0, true, false),
          new ExpectedGroundCell(17, 9, 0, true, false),
          new ExpectedGroundCell(16, 9, 0, true, false),
          new ExpectedGroundCell(15, 9, 0, true, false),
          new ExpectedGroundCell(14, 9, 0, true, false)
        ]),
      ]),
    ];
  }
}
