import {AbstractTestScenario} from "./abstract-test-scenario";
import {SplitDirectionDeltaPair} from "../split-direction-delta-pair";
import {DirectionDelta} from "../direction-delta";
import {MoveState} from "../test-models/move-state";
import {TrafficJamState} from "../test-models/traffic-jam-state";
import {ExpectedGroundCell} from "../test-models/expected-ground-cell";
import {Container} from "../container";

export class Split7 extends AbstractTestScenario {
  public getName(): string {
    return 'Split 7';
  }

  public getDescription(): string {
    return 'Jam rolls a split while waiting at a crossover';
  }

  public getJamConfig(): number[][] {
    return [
      [10, 15, 0, 3],
      [16, 6, 270, 12],
    ];
  }

  public override getDieRollSequence(): number[] {
    return [4, 1, 5, 1];
  }

  public override getSplitDirectionPairsComboSequence(): SplitDirectionDeltaPair[][] {
    return [
      [
        new SplitDirectionDeltaPair(DirectionDelta.makeAntiClockwise(), DirectionDelta.makeClockwise()),
      ],
    ];
  }

  protected makeContainer(): Container {
    return Container.makeStandard();
  }

  protected makeMoveStateAssertions(): MoveState[] {
    return [
      new MoveState(99, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 90, false, false),
          new ExpectedGroundCell(17, 12, 90, false, false),
          new ExpectedGroundCell(17, 13, 90, false, false),
          new ExpectedGroundCell(17, 14, 90, false, false),
          new ExpectedGroundCell(17, 15, 0, false, false),
          new ExpectedGroundCell(16, 15, 270, false, true),
          new ExpectedGroundCell(15, 15, 0, false, false),
          new ExpectedGroundCell(14, 15, 0, false, false),
          new ExpectedGroundCell(13, 15, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(19, 18, 0, true, false),
          new ExpectedGroundCell(18, 18, 0, false, false),
          new ExpectedGroundCell(17, 18, 270, false, false),
          new ExpectedGroundCell(17, 17, 270, false, false),
          new ExpectedGroundCell(17, 16, 270, false, false),
          new ExpectedGroundCell(17, 15, 0, false, false),
          new ExpectedGroundCell(16, 15, 270, false, true),
          new ExpectedGroundCell(15, 15, 0, false, false),
          new ExpectedGroundCell(14, 15, 0, false, false),
          new ExpectedGroundCell(13, 15, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(20, 19, 0, true, false),
          new ExpectedGroundCell(19, 19, 0, true, false),
          new ExpectedGroundCell(18, 19, 0, true, false),
          new ExpectedGroundCell(17, 19, 0, true, false),
          new ExpectedGroundCell(16, 19, 270, true, false),
          new ExpectedGroundCell(16, 18, 270, true, false),
          new ExpectedGroundCell(16, 17, 270, true, false),
          new ExpectedGroundCell(16, 16, 270, true, false),
          new ExpectedGroundCell(16, 15, 270, false, true),
          new ExpectedGroundCell(16, 14, 270, true, false),
          new ExpectedGroundCell(16, 13, 270, true, false),
          new ExpectedGroundCell(16, 12, 270, true, false),
          new ExpectedGroundCell(16, 11, 270, true, false),
        ]),
      ]),
      new MoveState(186, 3, [
        new TrafficJamState([
          new ExpectedGroundCell(17, 10, 90, true, false),
          new ExpectedGroundCell(17, 11, 90, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(19, 18, 0, true, false),
        ]),
        new TrafficJamState([
          new ExpectedGroundCell(25, 19, 0, true, false),
          new ExpectedGroundCell(24, 19, 0, true, false),
          new ExpectedGroundCell(23, 19, 0, true, false),
          new ExpectedGroundCell(22, 19, 0, true, false),
          new ExpectedGroundCell(21, 19, 0, true, false),
          new ExpectedGroundCell(20, 19, 0, true, false),
          new ExpectedGroundCell(19, 19, 0, true, false),
          new ExpectedGroundCell(18, 19, 0, true, false),
          new ExpectedGroundCell(17, 19, 0, true, false),
          new ExpectedGroundCell(16, 19, 270, true, false),
          new ExpectedGroundCell(16, 18, 270, true, false),
          new ExpectedGroundCell(16, 17, 270, true, false),
        ], [
          new ExpectedGroundCell(16, 16, null, null, null),
          new ExpectedGroundCell(16, 15, null, null, null),
          new ExpectedGroundCell(16, 14, null, null, null),
        ]),
      ]),
    ];
  }
}
