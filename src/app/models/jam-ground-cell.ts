import {TrafficJam} from "./traffic-jam";
import {GroundCell} from "./ground-cell";

export class JamGroundCell {
  private trafficJam: TrafficJam; // This is only relevant for the 1st JGC in a jam's list of JGCs.
  private readonly groundCell: GroundCell;

  private previousJamGroundCells: JamGroundCell[] = [];
  private previousJamGroundCellPointsIndex: number|null = null;
  private nextJamGroundCells: JamGroundCell[] = [];
  private nextJamGroundCellPointsIndex: number|null = null;

  private constructor(trafficJam: TrafficJam, cell: GroundCell) {
    this.trafficJam = trafficJam;
    this.groundCell = cell;
  }

  public static createFirstJamGroundCell(trafficJam: TrafficJam, cell: GroundCell): JamGroundCell {
    let newJamGroundCell: JamGroundCell = new JamGroundCell(trafficJam, cell);
    let firstJamGroundCell: JamGroundCell|null = trafficJam.getFirstJamGroundCell();

    // "Next" JGCs is an array of jam.firstJGC, or null if the jam doesn't have any JGCs.
    if (firstJamGroundCell) {
      newJamGroundCell.nextJamGroundCells = [firstJamGroundCell];
      newJamGroundCell.nextJamGroundCellPointsIndex = 0;
    }
    // First JGC in jam's "previous" cell, if there is one, (which should be null) needs to point to this one.
    if (firstJamGroundCell) {
      firstJamGroundCell.previousJamGroundCells.push(newJamGroundCell);
      firstJamGroundCell.previousJamGroundCellPointsIndex = firstJamGroundCell.previousJamGroundCells.length - 1;
    }

    trafficJam.setFirstJamGroundCell(newJamGroundCell);

    return newJamGroundCell;
  }

  public getGroundCell(): GroundCell {
    return this.groundCell;
  }

  public getPreviousJamGroundCell(): JamGroundCell|null {
    return this.previousJamGroundCellPointsIndex === null ? null : this.previousJamGroundCells[this.previousJamGroundCellPointsIndex];
  }

  public appendPreviousJamGroundCell(jamGroundCellToAppend: JamGroundCell): void {
    this.previousJamGroundCells.push(jamGroundCellToAppend);
    this.previousJamGroundCellPointsIndex = 0;
  }

  public getNextJamGroundCell(moveIndex: boolean = false): JamGroundCell|null {
    let result: JamGroundCell|null = null;

    if (this.nextJamGroundCellPointsIndex !== null) {
      result = this.nextJamGroundCells[this.nextJamGroundCellPointsIndex];
      if (moveIndex) {
        this.nextJamGroundCellPointsIndex = ++this.nextJamGroundCellPointsIndex % this.nextJamGroundCells.length;
      }
    }

    return result;
  }

  public appendNextJamGroundCell(jamGroundCellToAppend: JamGroundCell): void {
    this.nextJamGroundCells.push(jamGroundCellToAppend);
    this.nextJamGroundCellPointsIndex = this.nextJamGroundCells.length - 1;
  }

  public releaseThisAndAllAfter(shouldReleaseCell: boolean = true): void {
    for (let nextIndex: number = this.nextJamGroundCells.length - 1; nextIndex >= 0; nextIndex--) {
      this.nextJamGroundCells[nextIndex].releaseThisAndAllAfter();
    }

    if (shouldReleaseCell) {
      this.groundCell.releaseCell();
    }

    if (this.previousJamGroundCells.length) {
      do {
        this.previousJamGroundCells[0].clearNext(this);
        this.previousJamGroundCells.splice(0, 1);
      } while (this.previousJamGroundCells.length)
      this.previousJamGroundCellPointsIndex = null;
    } else {
      this.trafficJam.clearFirstJamGroundCell();
    }
  }

  private clearNext(forJamGroundCell: JamGroundCell): void {
    for (let nextIndex: number = 0; nextIndex < this.nextJamGroundCells.length; nextIndex++) {
      if (this.nextJamGroundCells[nextIndex] === forJamGroundCell) {
        if (this.nextJamGroundCellPointsIndex !== null && this.nextJamGroundCellPointsIndex >= nextIndex) {
          this.nextJamGroundCellPointsIndex = Math.max(this.nextJamGroundCellPointsIndex - 1, 0);
        }
        this.nextJamGroundCells.splice(nextIndex, 1);
      }
    }

    if (this.nextJamGroundCells.length === 0) {
      this.nextJamGroundCellPointsIndex = null;
    }
  }

  public setPreviousPoints(previousElement: JamGroundCell|null) {
    if (previousElement) {
      for (let previousIndex: number = 0; previousIndex < this.previousJamGroundCells.length; previousIndex++) {
        if (this.previousJamGroundCells[previousIndex] === previousElement) {
          this.previousJamGroundCellPointsIndex = previousIndex;
        }
      }
    }

    if (this.nextJamGroundCellPointsIndex !== null) {
      this.nextJamGroundCells[this.nextJamGroundCellPointsIndex].setPreviousPoints(this);
    }
  }

  public findJamGroundCellForGroundCell(groundCell: GroundCell): JamGroundCell|null {
    if (this.getGroundCell() === groundCell) {
      return this;
    }
    for (let nextIndex in this.nextJamGroundCells) {
      if (this.nextJamGroundCells.hasOwnProperty(nextIndex)) {
        let jamGroundCell: JamGroundCell|null = this.nextJamGroundCells[nextIndex].findJamGroundCellForGroundCell(groundCell);
        if (jamGroundCell !== null) {
          return jamGroundCell;
        }
      }
    }

    return null;
  }

  public makePointsSignature(): string {
    if (this.nextJamGroundCellPointsIndex === null) {
      return '';
    }

    let result: string = this.nextJamGroundCellPointsIndex.toString() + ':';

    for (let nextIndex in this.nextJamGroundCells) {
      if (this.nextJamGroundCells.hasOwnProperty(nextIndex)) {
        result += this.nextJamGroundCells[nextIndex].makePointsSignature();
      }
    }

    return result;
  }

  public getLeafNodes(leafNodesSoFar: JamGroundCell[] = []): JamGroundCell[] {
    if (this.nextJamGroundCells.length) {
      for (let nextIndex: number = 0; nextIndex < this.nextJamGroundCells.length; nextIndex++) {
        leafNodesSoFar = this.nextJamGroundCells[nextIndex].getLeafNodes(leafNodesSoFar);
      }
    } else {
      // If this cell is not already in the list, add it to the list.
      if (leafNodesSoFar.filter((jamGroundCell: JamGroundCell): boolean => jamGroundCell === this).length === 0) {
        leafNodesSoFar.push(this);
      }
    }

    return leafNodesSoFar;
  }

  public getNextJamGroundCells(): JamGroundCell[] {
    return this.nextJamGroundCells;
  }
}
