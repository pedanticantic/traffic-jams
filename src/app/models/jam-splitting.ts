import {TrafficJam} from "./traffic-jam";

export class JamSplitting {
  private otherJamIndex: number;
  private otherJam: TrafficJam;
  private switchToNextJam: boolean = false;

  constructor(otherJamIndex: number, otherJam: TrafficJam) {
    this.otherJamIndex = otherJamIndex;
    this.otherJam = otherJam;
  }

  public getOtherJamIndex(): number {
    return this.otherJamIndex;
  }

  public getOtherJam(): TrafficJam {
    return this.otherJam;
  }

  public setSwitchToNextJam(): void {
    this.switchToNextJam = true;
  }

  public clearSwitchingToNextJam(): void {
    this.switchToNextJam = false;
  }

  public isSwitchingToNextJam(): boolean {
    return this.switchToNextJam;
  }
}
