import {Direction} from "./direction";

export class DirectionDelta {
  private readonly angleDegrees: number;

  private constructor(angleDegrees: number) {
    // @TODO: Angle must be -90, 0, or 90 for now.
    this.angleDegrees = angleDegrees;
  }

  public static makeForwards(): DirectionDelta {
    return new DirectionDelta(0);
  }

  public static makeClockwise(): DirectionDelta {
    return new DirectionDelta(90);
  }

  public static makeAntiClockwise(): DirectionDelta {
    return new DirectionDelta(-90);
  }

  /**
   * This is not used, but it's here for completeness, plus it might be needed one day.
   */
  public isForward(): boolean {
    return this.angleDegrees === 0;
  }

  public isClockwise(): boolean {
    return this.angleDegrees === 90;
  }

  public isAntiClockwise(): boolean {
    return this.angleDegrees === -90;
  }

  public makeDirection(fromDirection: Direction): Direction {
    if (this.isAntiClockwise()) {
      return fromDirection.makeAntiClockwise();
    }

    if (this.isClockwise()) {
      return fromDirection.makeClockwise();
    }

    return fromDirection;
  }
}
