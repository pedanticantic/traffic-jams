import {GroundCell} from "./ground-cell";
import {Rectangle} from "./rectangle";

export class Container {
  private cells: GroundCell[][] = [];
  private containerLimits: Rectangle = new Rectangle(0, 0, 0, 0);

  constructor(initialShape: Rectangle) {
    this.addRectangle(initialShape);
  }

  public static makeStandard(): Container {
    return new Container(new Rectangle(0, 29, 0, 19));
  }

  private createEmptyRows(adjustingRectangle: Rectangle): void {
    // Make sure every row from this.containerLimits.getY2() to y2 (inclusive), if any, exists. We already know that
    // rows below limits.y2 will exist.
    for (let rowNum: number = this.containerLimits.getY2(); rowNum <= adjustingRectangle.getY2(); rowNum++) {
      if (! this.cells.hasOwnProperty(rowNum)) {
        this.cells[rowNum] = [];
      }
    }
  }

  public addRectangle(shapeToAdd: Rectangle): Container {
    this.createEmptyRows(shapeToAdd);

    // Add the block of cells.
    for (let rowNum: number = shapeToAdd.getY1(); rowNum <= shapeToAdd.getY2(); rowNum++) {
      for (let colNum: number = shapeToAdd.getX1(); colNum <= shapeToAdd.getX2(); colNum++) {
        if (! this.getCellAt(colNum, rowNum)) {
          this.cells[rowNum][colNum] = new GroundCell(colNum, rowNum);
        }
      }
    }

    // Refresh the limits rectangle.
    this.containerLimits = this.containerLimits.enclosingRectangle(shapeToAdd);

    return this;
  }

  public subtractRectangle(shapeToSubtract: Rectangle): Container {
    this.createEmptyRows(shapeToSubtract);

    // Subtract the block of cells.
    for (let rowNum: number = shapeToSubtract.getY1(); rowNum <= shapeToSubtract.getY2(); rowNum++) {
      for (let colNum: number = shapeToSubtract.getX1(); colNum <= shapeToSubtract.getX2(); colNum++) {
        if (this.getCellAt(colNum, rowNum)) {
          delete this.cells[rowNum][colNum];
        }
      }
    }

    // Refresh the limits rectangle.
    this.containerLimits = this.containerLimits.enclosingRectangle(shapeToSubtract);

    return this;
  }

  public getAvailableCell(): GroundCell|null {
    let availableCell: GroundCell|null;
    do {
      availableCell = this.getCellAt(
        Math.floor(Math.random() * (1 + this.containerLimits.getX2())),
        Math.floor(Math.random() * (1 + this.containerLimits.getY2()))
      );
    } while (availableCell === null || availableCell.isInJam());

    return availableCell;
  }

  public getCellAt(x: number, y: number): GroundCell|null {
    if (! this.cells.hasOwnProperty(y)) {
      return null;
    }
    if (! this.cells[y].hasOwnProperty(x)) {
      return null;
    }

    return this.cells[y][x];
  }

  public getRows(): GroundCell[][] {
    return this.cells;
  }

  public getRowCellIndexes(rowIndex: number): (GroundCell|null)[] {
    return [...Array(1 + this.containerLimits.getX2()).keys()]
      .map((cellX: number) => this.getCellAt(cellX, rowIndex));
  }
}
