import {GroundCell} from "./ground-cell";
import {TrafficJam} from "./traffic-jam";
import {Direction} from "./direction";
import {AbstractTestScenario} from "./test-scenarios/abstract-test-scenario";
import {MoveState} from "./test-models/move-state";
import {TrafficJamState} from "./test-models/traffic-jam-state";
import {ExpectedGroundCell} from "./test-models/expected-ground-cell";
import {ForwardPathCell} from "./forward-path-cell";
import {JamSplitting} from "./jam-splitting";
import {TestService} from "../services/test-service";
import {JamGroundCell} from "./jam-ground-cell";
import {Container} from "./container";

// @TODO: I wonder if this class should hold the instance of TestService?
export class SimulationState {
  private container: Container|null = null;
  private trafficJams: TrafficJam[] = [];
  private currentJamIndex: number = 0;
  private availableTestScenarios: AbstractTestScenario[] = [];
  private runAllTests: boolean = false;
  private currentTestScenario: AbstractTestScenario|null = null;
  private speedPercentage: number = /*800*/ 100;
  private moveStateAssertions: MoveState[][] = [];
  private currentJamStep: number = this.getDefaultCurrentJamStep();

  public setContainer(container: Container): void {
    this.container = container;
  }

  private getDefaultCurrentJamStep(): number {
    return 1;
  }

  public getCellAt(x: number, y: number): GroundCell|null {
    return this.container?.getCellAt(x, y) ?? null;
  }

  public getRows(): GroundCell[][] {
    return this.container?.getRows() ?? [];
  }

  public getRowCellIndexes(rowIndex: number): (GroundCell|null)[] {
    return this.container?.getRowCellIndexes(rowIndex) ?? [];
  }

  public getAvailableCell(): GroundCell|null {
    return this.container?.getAvailableCell() ?? null;
  }

  public setAvailableTestScenarios(testScenarios: AbstractTestScenario[]): void {
    this.availableTestScenarios = testScenarios;
  }

  public getTestScenarios(): AbstractTestScenario[]{
    return this.availableTestScenarios;
  }

  public setCurrentTestScenarioBySlug(slug: string): void {
    this.runAllTests = slug === TestService.testAllSlug;
    if (this.runAllTests) {
      this.currentTestScenario = this.availableTestScenarios[0];

      return;
    }

    this.currentTestScenario = null;
    for (let scenarioIndex in this.availableTestScenarios) {
      if (this.availableTestScenarios[scenarioIndex].getSlug() === slug) {
        this.currentTestScenario = this.availableTestScenarios[scenarioIndex];
        this.currentTestScenario.setRunning();
      }
    }
  }

  public setCurrentTestScenario(index: number): void {
    this.currentTestScenario = this.availableTestScenarios[index];
  }

  public getCurrentTestScenario(): AbstractTestScenario|null {
    return this.currentTestScenario;
  }

  public isRunningAllTests(): boolean {
    return this.runAllTests;
  }

  public getSpeedFactor(): number {
    return 100 / this.speedPercentage;
  }

  public speedUp(): void {
    let speedMap: number[] = [];
    speedMap[15] = 25;
    speedMap[8] = 15;
    speedMap[1] = 2;
    this.speedPercentage = speedMap.hasOwnProperty(this.speedPercentage) ? speedMap[this.speedPercentage] : this.speedPercentage * 2;
  }

  public slowDown(): void {
    let speedMap: number[] = [];
    speedMap[25] = 15;
    speedMap[15] = 8;
    speedMap[1] = 1;
    this.speedPercentage = speedMap.hasOwnProperty(this.speedPercentage) ? speedMap[this.speedPercentage] : this.speedPercentage / 2;
  }

  public goSlow(): void {
    this.speedPercentage = 25;
  }

  public getSpeedForDisplay(): string {
    return this.speedPercentage + '%';
  }

  public getJamCount(): number {
    return this.trafficJams.length;
  }

  public getCurrentJamIndex(): number {
    return this.currentJamIndex;
  }

  public getCurrentJam(): TrafficJam {
    return this.trafficJams[this.currentJamIndex];
  }

  public nextJam(): void {
    if (this.getCurrentJam().isSplitting()) {
      this.getCurrentJam().getJamSplitting()?.clearSwitchingToNextJam();
      // Need to flip to the "other" jam. The "?? current index" is there to keep PHP Storm happy.
      this.currentJamIndex = this.getCurrentJam().getJamSplitting()?.getOtherJamIndex() ?? this.currentJamIndex;
      this.getCurrentJam().findAndSetNextVehicle(false);

      return;
    }

    this.currentJamIndex += this.currentJamStep;
    this.currentJamStep = this.getDefaultCurrentJamStep();
    if (this.currentJamIndex >= this.trafficJams.length) {
      this.currentJamIndex = 0;
    }
    this.getCurrentJam().setPointsForThisJam();
  }

  public getSummary(): string {
    let summary: string = 'Moving jam #' + (1 + this.currentJamIndex);
    let lastDieRoll: number|null = this.getCurrentJam().getLastDieRoll();
    if (lastDieRoll) {
      summary += '; die roll: ' + lastDieRoll + ' (' + TrafficJam.getDieRollDescription(lastDieRoll) + ')';
      if (this.getCurrentJam().isSplitting()) {
        summary += '; splitting';
      } else {
        if (this.getCurrentJam().isMovingVehicles()) {
          summary += '; moving vehicles';
        }
        if (this.getCurrentJam().isWaitingForCrossover()) {
          summary += '; waiting for crossover';
        }
      }
    }

    return summary;
  }

  public createTrafficJam(spliceIn: boolean = false): TrafficJam {
    let trafficJam: TrafficJam = new TrafficJam();
    if (spliceIn) {
      this.trafficJams.splice(this.currentJamIndex + 1, 0, trafficJam);
    } else {
      this.trafficJams.push(trafficJam);
    }

    return trafficJam;
  }

  public initialisePath(
    trafficJam: TrafficJam,
    jamPath: ForwardPathCell[],
    addVehicle: boolean = true,
    isSplittingJam: boolean = false
  ): void {
    let groundCell: GroundCell;
    for (let jamPathIndex in jamPath) {
      groundCell = jamPath[jamPathIndex].getGroundCell();
      this.addGroundCellToJam(
        trafficJam,
        groundCell.getX(),
        groundCell.getY(),
        jamPath[jamPathIndex].getDirection(),
        addVehicle,
        isSplittingJam
      );
    }
  }

  public addGroundCellToJam(
    trafficJam: TrafficJam,
    x: number,
    y: number,
    direction: Direction,
    addVehicle: boolean = true,
    isSplittingJam: boolean = false
  ): GroundCell|null {
    let cell: GroundCell|null = this.container?.getCellAt(x, y) ?? null;
    if (cell === null) {
      return null; // @TODO: Ideally this should error in some way if it gets here.
    }

    cell.setDirection(direction);
    JamGroundCell.createFirstJamGroundCell(trafficJam, cell);

    if (addVehicle) {
      cell.addVehicle(trafficJam.getVehicleColour());
    }

    if (isSplittingJam && cell.isCrossover()) {
      cell.incrementAdditionalOccupants();
    }

    return cell;
  }

  public addForwardPathCellToJam(
    trafficJam: TrafficJam,
    forwardPathCell: ForwardPathCell,
    addVehicle: boolean = true,
    isSplittingJam: boolean = false
  ): GroundCell|null {
    return this.addGroundCellToJam(
      trafficJam,
      forwardPathCell.getGroundCell().getX(),
      forwardPathCell.getGroundCell().getY(),
      forwardPathCell.getDirection(),
      addVehicle,
      isSplittingJam
    )
  }

  public getJamCells(justJamId: number|null, avoidJamId: number|null): number[][] {
    let jamCells: number[][] = [];
    let groundCellCoordinates: number[][];

    for (let jamIndex in this.trafficJams) {
      if (justJamId !== null && justJamId.toString() !== jamIndex) {
        // @TODO: Do this better.
        continue;
      }
      if (avoidJamId !== null && avoidJamId.toString() === jamIndex) {
        // @TODO: Do this better.
        continue;
      }
      groundCellCoordinates = this.trafficJams[jamIndex].getGroundCellCoordinates()
      for (let cellIndex in groundCellCoordinates) {
        this.appendCellToList(jamCells, groundCellCoordinates, cellIndex);
      }
    }

    return jamCells;
  }

  private appendCellToList(jamCells: number[][], groundCellCoordinates: number[][], cellIndex: any): void {
    if (! jamCells[groundCellCoordinates[cellIndex][0]]) {
      jamCells[groundCellCoordinates[cellIndex][0]] = [];
    }
    jamCells[groundCellCoordinates[cellIndex][0]][groundCellCoordinates[cellIndex][1]] = 1;
  }

  public setMoveStateAssertions(index: number, moveStateAssertions: MoveState[]) {
    moveStateAssertions.sort((first: MoveState, second: MoveState) => first.getMoveNumber() - second.getMoveNumber());
    this.moveStateAssertions[index] = moveStateAssertions;
  }

  public getMoveStateOutcomes(index: number): MoveState[] {
    return this.moveStateAssertions.hasOwnProperty(index) ? this.moveStateAssertions[index] : [];
  }

  public getMoveStateAssertionsForMoveNumber(index: number, moveNumber: number): MoveState|null {
    let matches: MoveState[] = this.getMoveStateOutcomes(index)
      .filter((moveState: MoveState): boolean => moveState.getMoveNumber() === moveNumber);

    if (matches.length === 0) {
      return null;
    }

    return matches[0];
  }

  public getMoveStateAssertionsForScenario(testScenario: AbstractTestScenario): MoveState[] {
    return this.moveStateAssertions[testScenario.testIndex];
  }

  public dumpState(): void {
    for (let jamIndex in this.trafficJams) {
      console.log('Next jam (' + jamIndex + ')...');
      console.log('Cells state:', this.trafficJams[jamIndex].getGroundCellStates());
    }
  }

  public checkJamState(jamIndex: number, trafficJamState: TrafficJamState): boolean {
    let trafficJam: TrafficJam|null = this.trafficJams[jamIndex];
    if (trafficJam === null) {
console.log('Jam does not seem to exist');
      return false;
    }

    let result: boolean = true;

    let expectedGroundCells: ExpectedGroundCell[] = trafficJamState.getExpectedGroundCells();
    let expectedGroundCell: ExpectedGroundCell;
    for (let expectedCellIndex in expectedGroundCells) {
      expectedGroundCell = expectedGroundCells[expectedCellIndex];
      result = result && trafficJam.checkCellStateAt(expectedGroundCell.getX(), expectedGroundCell.getY(), expectedGroundCell);
    }

    let shouldNotHaveGroundCells: ExpectedGroundCell[] = trafficJamState.getShouldNotHaveGroundCells();
    let shouldNotHaveGroundCell: ExpectedGroundCell;
    for (let shouldNotHaveCellIndex in shouldNotHaveGroundCells) {
      shouldNotHaveGroundCell = shouldNotHaveGroundCells[shouldNotHaveCellIndex];
      result = result && trafficJam.shouldNotHaveCellAt(shouldNotHaveGroundCell.getX(), shouldNotHaveGroundCell.getY(), shouldNotHaveGroundCell);
    }

console.log('checkJamState: passed:', result);
    return result;
  }

  public forkJam(fromTrafficJam: TrafficJam, jamGroundCells: JamGroundCell[]): TrafficJam {
    let forkedTrafficJam: TrafficJam = this.createTrafficJam(true);
    // @ts-ignore fromTrafficJam.getFirstJamGroundCell() will never be null
    forkedTrafficJam.setFirstJamGroundCell(fromTrafficJam.getFirstJamGroundCell());

    fromTrafficJam.setJamSplitting(new JamSplitting(this.currentJamIndex + 1, forkedTrafficJam));
    forkedTrafficJam.setJamSplitting(new JamSplitting(this.currentJamIndex, fromTrafficJam));

    return forkedTrafficJam;
  }

  public handleForkingFinished(oneJam: TrafficJam): void {
    this.currentJamIndex = Math.max(this.currentJamIndex, oneJam.getJamSplitting()?.getOtherJamIndex() ?? 0);
  }

  public removingAllTrailingCellsWithNoVehicle(): void {
    for (let jamIndex in this.trafficJams) {
      this.trafficJams[jamIndex].removingTrailingCellsWithNoVehicle();
    }
  }

  public isLeadVehicle(cell: GroundCell): boolean {
    for (let jamIndex in this.trafficJams) {
      if (this.trafficJams[jamIndex].getFirstCell() === cell) {
        return true;
      }
    }

    return false;
  }

  public getJamNumber(cell: GroundCell): number {
    for (let jamIndex in this.trafficJams) {
      if (this.trafficJams[jamIndex].getFirstCell() === cell) {
        return 1 + parseInt(jamIndex);
      }
    }

    return 0;
  }

  public runTest(slug: string): void {
    window.location.href = '?slug=' + slug;
  }

  public runNormal(): void {
    window.location.href = '';
  }

  public removeJams(): void {
    for (let jamIndex in this.trafficJams) {
      this.trafficJams[parseInt(jamIndex)].releaseAllCells();
    }
    this.trafficJams = [];
    this.currentJamIndex = 0;
  }

  // @TODO: There might be a better way of doing this (like having a reference to the JGC in the GC - but that would be
  // @TODO: a lot of work).
  public findJamGroundCellForGroundCell(groundCell: GroundCell): JamGroundCell|null {
    for (let jamIndex in this.trafficJams) {
      if (this.trafficJams.hasOwnProperty(jamIndex)) {
        let jamGroundCell: JamGroundCell|null = this.trafficJams[jamIndex].getFirstJamGroundCell()?.findJamGroundCellForGroundCell(groundCell) ?? null;
        if (jamGroundCell) {
          return jamGroundCell;
        }
      }
    }

    return null;
  }

  public removeJam(jamToRemove: TrafficJam): void {
    // Find the jam in the list. Remove it. If the current jam pointer is larger than its index, decrement it.
    let jamIndex: string;
    for (jamIndex in this.trafficJams) {
      if (this.trafficJams.hasOwnProperty(jamIndex) && this.trafficJams[jamIndex] === jamToRemove) {
        this.trafficJams.splice(parseInt(jamIndex), 1);
        if (parseInt(jamIndex) <= this.currentJamIndex && this.currentJamIndex > 0) {
          this.currentJamIndex--;
        }

        this.currentJamStep = 0;

        return;
      }
    }
  }
}
