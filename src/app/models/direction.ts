// @TODO: Initially, the only allowed angles will be 0, 90, 180, 270, but a future enhancement will allow granularity
// @TODO: down to 1 degree.
export class Direction {
  private angleDegrees: number;

  constructor(angleDegrees: number) {
    this.angleDegrees = angleDegrees;
  }

  public static randomDirection(): Direction {
    return new Direction(90 * Math.floor(Math.random() * 4));
  }

  public getAngleDegrees(): number {
    return this.angleDegrees;
  }

  public makeClockwise(): Direction {
    let rightAngleDegrees: number = this.angleDegrees - 90;
    if (rightAngleDegrees < 0) {
      rightAngleDegrees += 360;
    }

    return new Direction(rightAngleDegrees);
  }

  public makeAntiClockwise(): Direction {
    let leftAngleDegrees: number = this.angleDegrees + 90;
    if (leftAngleDegrees >= 360) {
      leftAngleDegrees -= 360;
    }

    return new Direction(leftAngleDegrees);
  }
}
