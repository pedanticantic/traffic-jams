import {SimulationState} from "../models/simulation-state";
import {SearchHelper} from "./search-helper";
import {Direction} from "../models/direction";
import {ForwardPathCell} from "../models/forward-path-cell";
import {GroundCell} from "../models/ground-cell";
import {TrafficJam} from "../models/traffic-jam";
import {AbstractTestScenario} from "../models/test-scenarios/abstract-test-scenario";
import {MoveState} from "../models/test-models/move-state";
import {TrafficJamState} from "../models/test-models/traffic-jam-state";
import {SplitDirectionDeltaPair} from "../models/split-direction-delta-pair";
import {SimulationRunner} from "../models/simulation-runner";
import {Corner1} from "../models/test-scenarios/corner1";
import {Crossover1} from "../models/test-scenarios/crossover1";
import {Crossover2} from "../models/test-scenarios/crossover2";
import {Crossover3} from "../models/test-scenarios/crossover3";
import {Crossover4} from "../models/test-scenarios/crossover4";
import {Crossover5} from "../models/test-scenarios/crossover5";
import {Crossover6} from "../models/test-scenarios/crossover6";
import {Crossover7} from "../models/test-scenarios/crossover7";
import {Split1} from "../models/test-scenarios/split1";
import {Split2} from "../models/test-scenarios/split2";
import {Split3} from "../models/test-scenarios/split3";
import {Split4} from "../models/test-scenarios/split4";
import {Split5} from "../models/test-scenarios/split5";
import {Split6} from "../models/test-scenarios/split6";
import {Split7} from "../models/test-scenarios/split7";
import {Obstacles1} from "../models/test-scenarios/obstacles1";
import {Obstacles2} from "../models/test-scenarios/obstacles2";
import {Obstacles3} from "../models/test-scenarios/obstacles3";
import {Merge1} from "../models/test-scenarios/merge1";
import {Merge2} from "../models/test-scenarios/merge2";
import {Merge3} from "../models/test-scenarios/merge3";
import {Merge4} from "../models/test-scenarios/merge4";
import {Merge5} from "../models/test-scenarios/merge5";
import {Merge6} from "../models/test-scenarios/merge6";
import {Merge7} from "../models/test-scenarios/merge7";
import {Merge8} from "../models/test-scenarios/merge8";
import {Merge9} from "../models/test-scenarios/merge9";
import {Merge10} from "../models/test-scenarios/merge10";

export class TestService {
  private testMode: boolean|null = null;
  private testScenario: AbstractTestScenario|null = null;
  private dieRollSequence: number[] = [];
  private lastDieRoll: number|null = null;
  private splitDirectionPairsComboSequence: SplitDirectionDeltaPair[][] = [];
  private atMilestone: boolean = false;

  public static readonly testAllSlug: string = '_all';

  public isInTestMode(simulationState: SimulationState): boolean {
    if (this.testMode !== null) {
      return this.testMode;
    }

    this.testMode = true;
    // this.testMode = false;

    if (this.testMode) {
      if (simulationState.isRunningAllTests()) {
        let testScenarios: AbstractTestScenario[] = simulationState.getTestScenarios();
        let testScenario: AbstractTestScenario;
        let scenarioIndex: number;
        for (let scenarioKey in testScenarios) {
          scenarioIndex = parseInt(scenarioKey);
          testScenario = testScenarios[scenarioIndex];
          testScenario.testIndex = scenarioIndex;
          simulationState.setMoveStateAssertions(testScenario.testIndex, testScenario.getMoveStateAssertions());
        }
        this.testScenario = testScenarios[0];
      } else {
        this.testScenario = simulationState.getCurrentTestScenario();
        if (this.testScenario === null) {
          this.testMode = false;
        } else {
          simulationState.setMoveStateAssertions(this.testScenario.testIndex, this.testScenario.getMoveStateAssertions());
        }
      }
    }

    return this.testMode;
  }

  public getAvailableTestScenarios(): AbstractTestScenario[] {
    return [
      new Corner1(),
      new Crossover1(),
      new Crossover2(),
      new Crossover3(),
      new Crossover4(),
      new Crossover5(),
      new Crossover6(),
      new Crossover7(),
      new Split1(),
      new Split2(),
      new Split3(),
      new Split4(),
      new Split5(),
      new Split6(),
      new Split7(),
      new Obstacles1(),
      new Obstacles2(),
      new Obstacles3(),
      new Merge1(),
      new Merge2(),
      new Merge3(),
      new Merge4(),
      new Merge5(),
      new Merge6(),
      new Merge7(),
      new Merge8(),
      new Merge9(),
      new Merge10(),
    ];
  }

  public runInTestMode(simulationState: SimulationState, simulationRunner: SimulationRunner): boolean {
    if (this.testScenario === null) {
      return false; // It should never get here.
    }

    this.initialiseJams(simulationState, this.testScenario);

    if (simulationState.isRunningAllTests()) {
console.log('We are running all tests - running to next milestone');
      this.runToNextMilestone(simulationState, simulationRunner);

      return false;
    }

    return true;
    // return false;
  }

  private initialiseJams(simulationState: SimulationState, testScenario: AbstractTestScenario): void {
    simulationState.setContainer(testScenario.getContainer());
    this.makeJams(simulationState, testScenario.getJamConfig());
    this.dieRollSequence = testScenario.getDieRollSequence();
    this.splitDirectionPairsComboSequence = testScenario.getSplitDirectionPairsComboSequence();
  }

  private runToNextMilestone(simulationState: SimulationState, simulationRunner: SimulationRunner): void {
    this.atMilestone = false;
    let safetyNet: number = 3000;
    do {
      simulationRunner.nextMove(false);
    } while (--safetyNet > 0 && ! this.atMilestone);

    if (this.testScenario === null) {
      return;
    }
    // Get the current move state assertion (we need its status) and all subsequent ones.
    let remainingMoveStates: MoveState[] = this.testScenario.getMoveStateAssertions().filter(
      function (moveState: MoveState): boolean {
        return moveState.getMoveNumber() >= simulationRunner.getMoveCount();
      }
    )
    // If there are more milestones (and the last one passed): set the state of the next one to "in progress", and call
    // ourselves with a timer.
    if (remainingMoveStates.length > 1 && remainingMoveStates[0].hasPassed()) {
      // We take the one in index 1 because index 0 contains the current move state assertion.
      remainingMoveStates[1].setMoveOutcomeInProgress();
      setTimeout(() => this.runToNextMilestone(simulationState, simulationRunner), 1);

      return;
    }

    // Otherwise, find the next scenario, switch to that, reset everything, set its first milestone to "in progress"
    // and run ourselves with a timer.
    let testScenarios: AbstractTestScenario[] = simulationState.getTestScenarios();
    let nextTestScenarioIndex: number = 1 + this.testScenario.testIndex;
    if (testScenarios.hasOwnProperty(nextTestScenarioIndex)) {
      simulationState.setCurrentTestScenario(nextTestScenarioIndex);
      this.testScenario = simulationState.getCurrentTestScenario();
      if (this.testScenario === null) {
        return;
      }
      simulationState.removeJams();
      this.initialiseJams(simulationState, this.testScenario);
      simulationRunner.reset();
      this.testScenario.getMoveStateAssertions()[0].setMoveOutcomeInProgress();
      setTimeout(() => this.runToNextMilestone(simulationState, simulationRunner), 1);

      return;
    }
  }

  public nextDieRoll(simulationState: SimulationState): number|null {
    if (! this.isInTestMode(simulationState)) {
      return null;
    }

    if (this.dieRollSequence.length) {
      this.lastDieRoll = this.dieRollSequence.shift() ?? null;
    }

    return this.lastDieRoll;
  }

  private makeJams(simulationState: SimulationState, jamConfig: number[][]): void {
    let trafficJam: TrafficJam;
    let startCell: GroundCell|null;
    let jamPath: ForwardPathCell[];
    let groundCell: GroundCell;

    for (let jamIndex in jamConfig) {
      trafficJam = simulationState.createTrafficJam();
      startCell = simulationState.getCellAt(jamConfig[jamIndex][0], jamConfig[jamIndex][1]);
      if (startCell === null) {
        return; // It will never get here.
      }
      jamPath = new SearchHelper(simulationState).forwardPathSearch(jamConfig[jamIndex][3], startCell, new Direction(jamConfig[jamIndex][2]));
      for (let jamPathIndex in jamPath) {
        groundCell = jamPath[jamPathIndex].getGroundCell();
        simulationState.addGroundCellToJam(
          trafficJam,
          groundCell.getX(),
          groundCell.getY(),
          jamPath[jamPathIndex].getDirection()
        );
      }
    }
  }

  public checkMoveState(currentMove: number, simulationState: SimulationState): void {
    let moveStateAssertions: MoveState|null;

    let thisTestIndex: number = simulationState.getCurrentTestScenario()?.testIndex ?? 0;
    // See if there's a test coming up. If so, slow everything down.
    moveStateAssertions = simulationState.getMoveStateAssertionsForMoveNumber(thisTestIndex, currentMove + 3);
    if (moveStateAssertions !== null && moveStateAssertions.goSlow()) {
      simulationState.goSlow();
    }

    moveStateAssertions = simulationState.getMoveStateAssertionsForMoveNumber(thisTestIndex, currentMove);
    if (moveStateAssertions === null) {
      return;
    }

    this.atMilestone = true;
    simulationState.dumpState();

console.log('Jam count - expected, actual:', moveStateAssertions.getExpectedJamCount(), simulationState.getJamCount());
    if (moveStateAssertions.getExpectedJamCount() !== simulationState.getJamCount()) {
console.log('Jam count does not match');
      moveStateAssertions.setMoveOutcomeFail();

      return;
    }

    let trafficJamStates: TrafficJamState[] = moveStateAssertions.getTrafficJamStates();

    let passed: boolean = true;
    for (let jamIndex in trafficJamStates) {
      passed = passed && simulationState.checkJamState(parseInt(jamIndex), trafficJamStates[jamIndex]);
    }
    if (passed) {
      moveStateAssertions.setMoveOutcomePass();
    } else {
      moveStateAssertions.setMoveOutcomeFail();
    }
  }

  public getNextSplitDirectionDeltaPairs(): SplitDirectionDeltaPair[] {
    if (this.splitDirectionPairsComboSequence.length) {
      return this.splitDirectionPairsComboSequence.shift() ?? [];
    }

    return [];
  }
}
