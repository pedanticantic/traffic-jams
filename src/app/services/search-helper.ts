import {SimulationState} from "../models/simulation-state";
import {GroundCell} from "../models/ground-cell";
import {Direction} from "../models/direction";
import {ForwardPathCell} from "../models/forward-path-cell";
import {SearchType} from "../models/search-type";

export class SearchHelper {
  simulationState: SimulationState;
  private probed: number[][] = [];
  private avoid: number[][] = [];
  private targets: number[][] = [];

  constructor(simulationState: SimulationState) {
    this.simulationState = simulationState;
  }

  public forwardPathSearch(
    numSteps: number,
    startCell: GroundCell,
    initialDirection: Direction,
    coordinatesToAvoid: number[][] = []
  ): ForwardPathCell[] {
    // Initialise the cells to avoid.
    this.avoid = this.simulationState.getJamCells(null, null);
    for (let avoidIndex in coordinatesToAvoid) {
      if (! this.avoid.hasOwnProperty(coordinatesToAvoid[avoidIndex][0])) {
        this.avoid[coordinatesToAvoid[avoidIndex][0]] = [];
      }
      this.avoid[coordinatesToAvoid[avoidIndex][0]][coordinatesToAvoid[avoidIndex][1]] = 1;
    }
    this.targets = [];

    return this.search(SearchType.makeForwardPathSearchType(numSteps), startCell, initialDirection);
  }

  public searchForJamToCrossOver(startCell: GroundCell, initialDirection: Direction): ForwardPathCell[] {
    this.avoid = this.simulationState.getJamCells(this.simulationState.getCurrentJamIndex(), null);
    this.targets = this.simulationState.getJamCells(null, this.simulationState.getCurrentJamIndex());

    return this.search(SearchType.makeSearchForCrossoverType(), startCell, initialDirection);
  }

  public searchForJamToMergeInTo(startCell: GroundCell, initialDirection: Direction): ForwardPathCell[] {
    this.avoid = this.simulationState.getJamCells(this.simulationState.getCurrentJamIndex(), null);
    this.targets = this.simulationState.getJamCells(null, this.simulationState.getCurrentJamIndex());

    return this.search(SearchType.makeSearchForMergeType(), startCell, initialDirection);
  }

  private search(searchType: SearchType, fromCell: GroundCell, startDirection: Direction): ForwardPathCell[] {
    this.probed = [];
    let queue: ForwardPathCell[][] = [];

    let currentCell: GroundCell = fromCell;
    let currentDirection: Direction = startDirection;
    let candidateCell: GroundCell|null;
    queue.push([new ForwardPathCell(currentCell, currentDirection)]);
    let nextForwardPathCells: ForwardPathCell[]|undefined;
    let backwardsDirection: Direction;
    let rightDirection: Direction;
    let leftDirection: Direction;
    let longestPathSeen: ForwardPathCell[] = [];

    let safetyNet: number = 1000;
    do {
      safetyNet--;

      // Get the next candidate off the queue. We have to jump through hoops because Angular thinks the next cell could
      // be null when we know it can't be.
      if (searchType.hasRequiredLength()) {
        nextForwardPathCells = queue.pop();
      } else {
        nextForwardPathCells = queue.shift();
      }
      if (nextForwardPathCells === undefined) {
        return [];
      }
      if (searchType.hasRequiredLength() && nextForwardPathCells.length === (1 + searchType.getRequiredLength())) {
        nextForwardPathCells.shift(); // @TODO: Check if we can do anything about this (the start cell is the first cell in the path).

        return nextForwardPathCells; // Success!!
      }
      currentCell = nextForwardPathCells[nextForwardPathCells.length - 1].getGroundCell();
      currentDirection = nextForwardPathCells[nextForwardPathCells.length - 1].getDirection();

      // Check if we've been here before.
      if (this.probed[currentCell.getX()]) {
        if (this.probed[currentCell.getX()][currentCell.getY()]) {
          continue;
        }
      }

      // Check if this is a target cell.
      if (this.isTargetCell(currentCell)) {
        candidateCell = null;
        if (searchType.shouldGoOnePastTarget()) {
          candidateCell = this.getCandidateCell(currentCell, currentDirection);
          if (candidateCell === null || this.isTargetCell(candidateCell)) {
            continue;
          }
        }
        // We have hit a target cell. Remove the first cell from the path.
        nextForwardPathCells.shift(); // @TODO: Check if we can do anything about this (the start cell is the first cell in the path).
        // candidateCell will always be non-null if we should go 1 past target; this is for SA.
        if (searchType.shouldGoOnePastTarget() && candidateCell !== null) {
          // Remove the last cell from the return path.
          nextForwardPathCells.pop();
          // Add in the crossover. getDirection() will never be null, but we have to fool the SA.
          nextForwardPathCells.push(new ForwardPathCell(currentCell, currentCell.getDirection() ?? new Direction(0)));
          // Add in the cell "past" the crossover.
          nextForwardPathCells.push(new ForwardPathCell(candidateCell, currentDirection));
        }

        return nextForwardPathCells;
      }

      // If this is the longest path we've seen, remember it.
      if (nextForwardPathCells.length > 1 + longestPathSeen.length) {
        longestPathSeen = nextForwardPathCells.map((onePathCell: ForwardPathCell) => onePathCell);
      }

      // Note that we've been here before.
      if (! this.probed[currentCell.getX()]) {
        this.probed[currentCell.getX()] = [];
      }
      this.probed[currentCell.getX()][currentCell.getY()] = 1;

      // Try moving backwards, then right, then left, then forward. For each one, if we can move that way, push it on
      // to the queue. We try all 4 directions because we might be looking to the right of the original direction, so
      // "backwards" is actually left of the original direction.
      // Backwards.
      backwardsDirection = currentDirection.makeClockwise().makeClockwise();
      candidateCell = this.getCandidateCell(currentCell, backwardsDirection);
      if (candidateCell) {
        queue.push(this.makePath(nextForwardPathCells, new ForwardPathCell(candidateCell, backwardsDirection)));
      }
      // Right.
      rightDirection = currentDirection.makeClockwise();
      candidateCell = this.getCandidateCell(currentCell, rightDirection);
      if (candidateCell) {
        queue.push(this.makePath(nextForwardPathCells, new ForwardPathCell(candidateCell, rightDirection)));
      }
      // Left.
      leftDirection = currentDirection.makeAntiClockwise();
      candidateCell = this.getCandidateCell(currentCell, leftDirection);
      if (candidateCell) {
        queue.push(this.makePath(nextForwardPathCells, new ForwardPathCell(candidateCell, leftDirection)));
      }
      // Forward.
      candidateCell = this.getCandidateCell(currentCell, currentDirection);
      if (candidateCell) {
        queue.push(this.makePath(nextForwardPathCells, new ForwardPathCell(candidateCell, currentDirection)));
      }

    } while (queue.length > 0 && safetyNet > 0);

    if (! searchType.hasRequiredLength()) {
      return []; // We weren't able to hit any targets.
    }

    longestPathSeen.shift();

    return longestPathSeen;
  }

  private isTargetCell(groundCell: GroundCell): boolean {
    if (this.targets[groundCell.getX()]) {
      if (this.targets[groundCell.getX()][groundCell.getY()]) {
        return true;
      }
    }

    return false;
  }

  private getCandidateCell(fromCell: GroundCell, currentDirection: Direction): GroundCell|null {
    let angleDegrees: number = currentDirection.getAngleDegrees();
    let x: number = fromCell.getX();
    let y: number = fromCell.getY();
    let dx: number = angleDegrees === 0 ? 1 : (angleDegrees === 180 ? -1 : 0);
    let dy: number = angleDegrees === 90 ? -1 : (angleDegrees === 270 ? 1 : 0);

    let candidateCell: GroundCell|null = this.simulationState.getCellAt(x + dx, y + dy);
    if (candidateCell === null) {
      return null;
    }
    if (this.avoid[candidateCell.getX()]) {
      if (this.avoid[candidateCell.getX()][candidateCell.getY()]) {
        return null;
      }
    }

    return candidateCell;
  }

  private makePath(forwardPathCells: ForwardPathCell[], nextCell: ForwardPathCell): ForwardPathCell[] {
    let pathSoFar: ForwardPathCell[] = forwardPathCells.map((onePathCell: ForwardPathCell) => onePathCell);
    pathSoFar.push(nextCell);

    return pathSoFar;
  }
}
